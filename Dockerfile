FROM python:latest
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN mkdir /static/
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD ./biohome /code/
ADD ./media /media/
EXPOSE 80
