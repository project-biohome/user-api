<div align="center"><img src="https://i.imgur.com/AV7b4Ja.png" /></div>

# Project Biohome

**Biohome** is a modular indoor greenhouse that makes apartment culture simple and accessible. It allows users to grow plants and herbs for those who can't have access to a balcony or a garden.

See **HOWTO.md** to learn how to launch the server as well as user's dashboard.

### This project is no longer maintained.

## Project structure
### Software
- A **web dashboard** and a **REST API** developed in Python with [**Django**](https://www.djangoproject.com/ "Django") framework (see [user-api](https://gitlab.com/project-biohome/user-api "user-api"))
- An **Android app** as well as an **iOS app** used to control the greenhouse remotely
- An **embedded API** developed in Python used to communicate sensors data from the greenhouse to the server

### Hardware
- An embedded **Arduino UNO WiFi** used to communicate data from the greenhouse to the server with [MQTT](https://pypi.org/project/paho-mqtt/ "MQTT") protocol
- The greenhouse itself with some modules and sensors

## Screenshots

<div align="center">
    <img src="https://i.imgur.com/cRBv48A.jpg" />
    <p><i>3D model of Biohome greenhouse</i></p>
    <br />
    <img src="https://i.imgur.com/ZvPUubD.gif" />
    <p><i>Biohome user's dashboard</i></p>
</div>

<div align="center">    
    
</div>

## Authors
- **Alexis Bevillard**
- **Louis Bouteillon**
- **Thomas Cantin**
- **Aymeric Cesar**
- **Julien Chardon**
- **Mathieu Chassara**
- **Valentin Potier**
- **Thomas Trupin**
- **Benjamin Vibrac**
