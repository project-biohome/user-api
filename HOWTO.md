# user-api

user-api is both front & back website of the Biohome project. It contains user's dashboard to access to greenhouses as well as the REST API used to communicate between greenhouses and user apps.

## Prerequisites

- git

`sudo apt-get install git`

- python3

`sudo apt-get install python3`

- python3-pip

`sudo apt-get install python3-pip`

- virtualenv

`sudo apt-get install virtualenv`

## Installation

1. Clone the project repository and create a virtual environment
```
> git clone https://gitlab.com/project-biohome/user-api.git
> cd user-api
> virtualenv -p python3 venv
```

2. Edit the `activate` script inside bin folder to set the environment variable 
`DJANGO_SETTINGS_MODULE` to 'dev_settings.py'
```
DJANGO_SETTINGS_MODULE="biohome.dev_settings"
export DJANGO_SETTINGS_MODULE
```

3. Activate the virtual environment and install the requirements
```
> source ./venv/bin/activate (Linux)
> venv\Scripts\activate (Windows)
> pip install -r requirements.txt
```

## Before you can start

To create a superuser account, you can use the manage.py script like so:
```
> cd biohome
> ./manage.py migrate
> ./manage.py createsuperuser
> ./manage.py runserver
```

## Built With
* [Python 3.5](https://www.python.org/ "Python 3.5") - Programming language
* [Django](https://www.djangoproject.com/ "Django") - High level Python web framework
* [Bootstrap](https://getbootstrap.com/) - HTML/CSS/JS responsive toolkit

## Troubleshooting
If you encounter problems during installation, please fill an issue [here](https://gitlab.com/project-biohome/user-api/issues).
