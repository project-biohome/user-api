from django.forms import widgets

class TextInput(widgets.TextInput):
    template_name = "textinput.html"

class Textarea(widgets.Textarea):
    template_name = "textarea.html"

class PasswordInput(widgets.PasswordInput):
    template_name = "passwordinput.html"

class EmailInput(widgets.EmailInput):
    template_name = "emailinput.html"

class SelectMultiple(widgets.SelectMultiple):
    template_name = "selectmultiple.html"

class Select(widgets.Select):
    template_name = "select.html"

class FileInput(widgets.FileInput):
    template_name = "fileinput.html"

class SelectDate(widgets.SelectDateWidget):
    template_name = 'selectdate.html'

class CheckboxInput(widgets.CheckboxInput):
    template_name = 'checkboxinput.html'