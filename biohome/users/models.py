from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

class User(AbstractUser):
    email = models.EmailField(_('email address'), blank=False)

    first_name = models.CharField(null=True, blank=True, max_length=64)
    last_name = models.CharField(null=True, blank=True, max_length=64)

    def natural_key(self):
        return {
            'username': self.username,
            'email': self.email,
            'id': self.pk,
        }
