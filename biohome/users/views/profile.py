from django.contrib.auth import get_user_model, get_user
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView
from django.views.generic import RedirectView, ListView

from json_serializer import JSONSerializer
from users.models import User

import json

class ProfileDetailRedirect(LoginRequiredMixin, RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse('profile_detail', args=[self.request.user.id])

class UserList(LoginRequiredMixin, ListView):
    fields = [
        'username',
        'email',
        'first_name',
        'last_name'
    ]
    user_model = get_user_model()
    template_name = 'profile/users_list.html'

    def get(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            users = self.user_model.objects.all()
            data = JSONSerializer().serialize(users, fields=self.fields)
            parsed = json.loads(data)
            return JsonResponse(parsed, safe=False)

        return super().get(self, request, *args, **kwargs)

    def get_queryset(self):
        search = self.request.GET.get('search', None)
        if search:
            return self.user_model.objects.filter(username__startswith=search)
        return self.user_model.objects.all()

class UserDetail(LoginRequiredMixin, DetailView):
    model = get_user_model()
    fields = [
        'username',
        'email',
        'first_name',
        'last_name'
    ]
    template_name = 'profile/profile_details.html'

    def get(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            try:
                user_model = get_user_model()
                user = user_model.objects.get(pk=self.kwargs.get('pk'))
                data = JSONSerializer().serialize([user], fields=self.fields)
                parsed = json.loads(data)
                return JsonResponse(parsed, safe=False)
            except ObjectDoesNotExist:
                return JsonResponse({'error': 'User does not exist'}, status=400)

        return super().get(self, request, *args, **kwargs)


class ProfileUpdate(LoginRequiredMixin, UpdateView):
    user_model = get_user_model()
    fields = [
        'email',
        'first_name',
        'last_name'
    ]
    template_name = 'profile/profile_edit.html'

    def get_object(self, queryset=None):
        return get_object_or_404(self.model, pk=self.request.user.id)