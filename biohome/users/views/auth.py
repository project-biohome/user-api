from django.conf import settings
from django.contrib.auth import login as auth_login, logout as auth_logout, get_user_model
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import Group
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.views import (
    LoginView, LogoutView,
    PasswordChangeView, PasswordChangeDoneView,
    PasswordResetView, PasswordResetDoneView,
    PasswordResetConfirmView, PasswordResetCompleteView
)
from django.core.mail import EmailMessage
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse, HttpResponseRedirect
from django.middleware.csrf import CSRF_SESSION_KEY, get_token
from django.shortcuts import render, resolve_url
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic.base import TemplateView

from json_serializer import errors_to_json
from users.forms import (
    SignupForm,
    CustomPasswordChangeForm, CustomPasswordResetForm, CustomSetPasswordForm
)
from users.tokens import account_activate_token

import json

UserModel = get_user_model()

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.LoginView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/LoginView/
class Login(LoginView):

    form_class = AuthenticationForm
    template_name = 'users/login.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if not request.META.get('HTTP_ACCEPT') == 'application/json':
                return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))
            else:
                return JsonResponse({'error': 'user already logged in'}, status=400)
        else:
            return super(Login, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = AuthenticationForm(request=request, data=data)

        if form.is_valid():
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                auth_login(self.request, form.get_user())
                return JsonResponse({
                    'response': 'logged in',
                    'session_cookie_name': settings.SESSION_COOKIE_NAME,
                    'user_id': form.get_user().id
                }, status=200)
            else:
                return self.form_valid(form)
        else:
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'error': errors_to_json(form.errors)
                }, status=400)
            else:
                return self.form_invalid(form)

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.LogoutView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/LogoutView/
class Logout(LogoutView):
    template_name = 'users/logout.html'

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        auth_logout(request)
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            return JsonResponse({
                'response' : 'logged out'
            })
        else:
            next_page = self.get_next_page()
            if next_page:
                # Redirect to this page until the session has been cleared.
                return HttpResponseRedirect(next_page)

def signup(request):
    if request.user.is_authenticated:
        if not request.META.get('HTTP_ACCEPT') == 'application/json':
            return HttpResponseRedirect(resolve_url(settings.LOGIN_REDIRECT_URL))
        else:
            return JsonResponse({'error': 'user already logged in'}, status=400)

    if request.method == 'POST':
        form = SignupForm(request.POST)

        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = SignupForm(data=data)

        if not form.is_valid():
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({'error': errors_to_json(form.errors)}, status=400)

        else:
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            current_site = get_current_site(request)
            subject = 'Welcome to Biohome !'
            message = render_to_string('users/account_activate_email_sent.html', {
                'user': user,
                'domain': current_site.domain,
                'protocol': request.scheme,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activate_token.make_token(user),
                'site_name': "Biohome"
            })
            email = EmailMessage(subject, message, to=[form.cleaned_data.get('email')])
            email.send()

            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'response': 'registered successfully, confirmation email sent',
                    'session_cookie_name': settings.SESSION_COOKIE_NAME,
                }, status=200)
            else:
                return render(request, 'users/signup_done.html')
    else:
        form = SignupForm()

    return render(request, 'users/signup.html', {'form': form})

class AccountActivate(TemplateView):

    template_name = 'users/account_activate_done.html'
    account_activated = False

    def get_user(self, uidb64):
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user = UserModel.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None
        return user

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        assert 'uidb64' in kwargs and 'token' in kwargs

        self.account_activated = False
        self.user = self.get_user(kwargs['uidb64'])

        if self.user is not None:
            token = kwargs['token']
            if token == INTERNAL_ACTIVATION_URL_TOKEN:
                session_token = self.request.session.get(INTERNAL_ACTIVATION_SESSION_TOKEN)
                if account_activate_token.check_token(self.user, session_token):
                    self.user.is_active = True
                    self.user.groups.add(Group.objects.get(name=settings.USER_GROUP_NAME))
                    self.user.save()
                    self.account_activated = True
                    del self.request.session[INTERNAL_ACTIVATION_SESSION_TOKEN]
                    return super(AccountActivate, self).dispatch(request, *args, **kwargs)
            else:
                if account_activate_token.check_token(self.user, token):
                    self.request.session[INTERNAL_ACTIVATION_SESSION_TOKEN] = token
                    redirect_url = self.request.path.replace(token, INTERNAL_ACTIVATION_URL_TOKEN)
                    return HttpResponseRedirect(redirect_url)

        return super(AccountActivate, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountActivate, self).get_context_data(**kwargs)
        if self.account_activated:
            context['account_activated'] = True
        else:
            context.update({
                'account_activated': False
            })
        return context

INTERNAL_ACTIVATION_URL_TOKEN = 'activate-account'
INTERNAL_ACTIVATION_SESSION_TOKEN = '_account_activate_token'

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordChangeView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordChangeView/
class PasswordChange(PasswordChangeView):

    form_class = CustomPasswordChangeForm
    success_url = reverse_lazy('password_change_done')
    template_name = 'users/password_change.html'

    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            form = self.get_form()
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': errors_to_json(form.errors)}, status=400)
        return super().post(request, *args, **kwargs)

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordChangeDoneView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordChangeDoneView/
class PasswordChangeDone(PasswordChangeDoneView):

    template_name = 'users/password_change_done.html'

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordResetView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordResetView/
class PasswordReset(PasswordResetView):

    form_class = CustomPasswordResetForm
    success_url = reverse_lazy('password_reset_done')
    template_name = 'users/password_reset.html'

    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            form = self.get_form()
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': errors_to_json(form.errors)}, status=400)
        return super().post(request, *args, **kwargs)

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordResetDoneView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordResetDoneView/
class PasswordResetDone(PasswordResetDoneView):

    template_name = 'users/password_reset_done.html'

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordResetConfirmView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordResetConfirmView/
class PasswordResetConfirm(PasswordResetConfirmView):

    form_class = CustomSetPasswordForm
    success_url = reverse_lazy('password_reset_complete')
    template_name = 'users/password_reset_confirm.html'

    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            form = self.get_form()
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': errors_to_json(form.errors)}, status=400)
        return super().post(request, *args, **kwargs)

# See : https://docs.djangoproject.com/en/1.11/topics/auth/default/#django.contrib.auth.views.PasswordResetCompleteView
# See also : https://ccbv.co.uk/projects/Django/1.11/django.contrib.auth.views/PasswordResetCompleteView/
class PasswordResetComplete(PasswordResetCompleteView):

    template_name = 'users/password_reset_complete.html'

def get_csrf_token(request):

    if request.method == 'GET':
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            csrftoken = request.session.get(CSRF_SESSION_KEY)
            if csrftoken is None:
                csrftoken = get_token(request)
            return JsonResponse({
                'csrf': csrftoken
            }, status=200)
    raise PermissionDenied