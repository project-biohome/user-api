from django.conf.urls import url, include
from django.views.generic import RedirectView

from .views import (
    Login, Logout, signup, get_csrf_token,
    AccountActivate,
    PasswordChange, PasswordChangeDone,
    PasswordReset, PasswordResetDone,
    PasswordResetConfirm, PasswordResetComplete,
    UserDetail
)
from users.views.profile import UserList

auth_patterns = [
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^signup/$', signup, name='signup'),

    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        AccountActivate.as_view(), name='account_activate'),

    url(r'^account/password/change/$',
        PasswordChange.as_view(), name='password_change'),
    url(r'^account/password/change/done/$',
        PasswordChangeDone.as_view(), name='password_change_done'),

    url(r'^account/password/reset/$',
        PasswordReset.as_view(), name='password_reset'),
    url(r'^account/password/reset/done/$',
        PasswordResetDone.as_view(), name='password_reset_done'),

    url(r'^password/reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        PasswordResetConfirm.as_view(), name='password_reset_confirm'),
    url(r'^password/reset/done/$',
        PasswordResetComplete.as_view(), name='password_reset_complete')
]

#   Profile patterns :
#       - Users list (/users/)
#       - User details (/users/<pk>/ ou /users/<pk>/details/)
#       - Profile edit (/users/<pk>/edit/)
profile_patterns = [
    url(r'^$', UserList.as_view(), name='users_list'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name='user_details'), name='user_redirect'),
    url(r'^(?P<pk>\d+)?/details/$', UserDetail.as_view(), name='user_details'),
    #url(r'^(?P<pk>\d+)?/edit/$', ProfileUpdate.as_view(), name='user_edit')
]

urlpatterns = [
    url(r'^', include(auth_patterns)),
    url(r'^get-token/$', get_csrf_token, name='get_csrf_token'),
    url(r'^users/', include(profile_patterns)),
]