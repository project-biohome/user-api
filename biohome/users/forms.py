from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm
from django.contrib.auth import get_user_model
from django import forms

from widgets import widgets

class SignupForm(UserCreationForm):
    email = forms.EmailField(required=True)

    first_name = forms.CharField(required=False, max_length=64)
    last_name = forms.CharField(required=False, max_length=64)

    class Meta:
        model = get_user_model()
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']

        widgets = {
            'username': widgets.TextInput,
            'email': widgets.EmailInput,
            'first_name': widgets.TextInput,
            'last_name': widgets.TextInput,
            'password1': widgets.PasswordInput,
            'password2': widgets.PasswordInput,
        }

    def clean_email(self):
        model = get_user_model()
        email = self.cleaned_data.get('email')
        try:
            match = model.objects.get(email=email)
        except model.DoesNotExist:
            return email
        raise forms.ValidationError('A user with that email already exists.', 'email_already_exists')

class CustomPasswordChangeForm(PasswordChangeForm):

    def __init__(self, *args, **kwargs):
        super(CustomPasswordChangeForm, self).__init__(*args, **kwargs)
        for field in ('old_password', 'new_password1', 'new_password2'):
            self.fields[field].widget.attrs = {'class': 'form-control'}


class CustomPasswordResetForm(PasswordResetForm):

    def __init__(self, *args, **kwargs):
        super(CustomPasswordResetForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs = {'class': 'form-control'}

class CustomSetPasswordForm(SetPasswordForm):

    def __init__(self, *args, **kwargs):
        super(CustomSetPasswordForm, self).__init__(*args, **kwargs)
        self.fields['new_password1'].widget.attrs = {'class': 'form-control'}
        self.fields['new_password2'].widget.attrs = {'class': 'form-control'}