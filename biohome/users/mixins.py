from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse, HttpResponseForbidden

class BiohomePermissionMixin(PermissionRequiredMixin):

    def handle_no_permission(self):
        if self.raise_exception:
            raise PermissionDenied(self.get_permission_denied_message())

        if self.request.META.get('HTTP_ACCEPT') == 'application/json':
            return JsonResponse({
                'response': 'permission forbidden'
            }, status=403)
        else:
            return HttpResponseForbidden()

    def has_permission(self):
        return True
