from __future__ import unicode_literals

from django.http import JsonResponse
from json_serializer import JSONSerializer

from machina.apps.forum.views import (
    IndexView as BaseIndexView,
    ForumView as BaseForumView,
)

import json

class IndexView(BaseIndexView):
    def get(self, request, *args, **kwargs):
        ret = super().get(request, *args, **kwargs)
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = [
                'direct_topics_count',
                'direct_posts_count',
                'last_post_on',
                'parent',
                'created',
                'updated',
                'name',
                'image',
                'type',
                'description',
                'slug',
            ]
            result = JSONSerializer().serialize(self.get_queryset().forums, fields=fields)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return ret

class ForumView(BaseForumView):
    def get(self, request, *args, **kwargs):
        ret = super().get(request, *args, **kwargs)
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = [
                'forum',
                'subject',
                'created',
                'updated',
                'last_post_on',
                'type',
                'posts_count',
                'slug',
            ]
            queryset = self.get_forum().topics.select_related('poster', 'last_post', 'last_post__poster')
            result = JSONSerializer().serialize(queryset, fields=fields)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return ret
