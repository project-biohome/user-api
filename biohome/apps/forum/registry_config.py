from machina.apps.forum.registry_config import ForumRegistryConfig as BaseForumRegistryConfig

class ForumRegistryConfig(BaseForumRegistryConfig):
    name = 'apps.forum'
