# -*- coding: utf-8 -*-

from __future__ import unicode_literals

# Custom models should be declared before importing
# django-machina models

from machina.apps.forum_conversation.models import *  # noqa
