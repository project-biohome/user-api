from __future__ import unicode_literals

from django.http import JsonResponse
from json_serializer import JSONSerializer

from machina.apps.forum_conversation.views import (
    TopicView as BaseTopicView,
)

import json

class TopicView(BaseTopicView):
    def get(self, request, *args, **kwargs):
        ret = super().get(request, *args, **kwargs)
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = [
                'topic',
                'content',
                'created',
                'updated',
                'username',
                'subject',
            ]
            result = JSONSerializer().serialize(self.get_queryset(), fields=fields)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return ret
