from machina.apps.forum_conversation.registry_config import ConversationRegistryConfig as BaseConversationRegistryConfig

class ConversationRegistryConfig(BaseConversationRegistryConfig):
    name = 'apps.forum_conversation'
