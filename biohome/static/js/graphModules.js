const        Y_STEP = 5;

function    closestMultiple(number, multiple)
{
    return Math.ceil(number / multiple) * multiple;
}

function    findHighestY(array)
{
    let arrayTemp = [];

    for (i = 1; i < array.length; i += 2)
    {
    arrayTemp.push(array[i]);
    }
    return Math.max.apply(null, arrayTemp);
}

function    findLowestY(array, multiple)
{
    let        lowest = Math.min.apply(null, array);
    return    Math.floor(lowest / multiple) * multiple;
}

$( document ).ready
(
    function()
    {
        var dataDiv = document.getElementsByName('readings');

        dataDiv.forEach(function(element)
        {
            let dataGraph = [[[0, 0]]];
            let tempArray = JSON.parse(element.dataset.values);
            let maxY = findHighestY(tempArray);
    
    
            let j = 0;
            for (i=0; i < tempArray.length; i += 2)
            {
                dataGraph[0][j] = [tempArray[i], tempArray[i + 1]];
                j++;
            }
    
            var chart = $.jqplot(element.dataset.name, dataGraph,
            {
                animate: true,
                axes: {
                    yaxis: {
                        min: findLowestY(tempArray, Y_STEP),
                        max: closestMultiple(maxY, Y_STEP),
                        tickInterval: Y_STEP,
                        label: "Value (relative)",
                        labelRenderer: $.jqplot.CanvasAxisLabelRenderer
                    },
                    xaxis: {
                        min:0, max:tempArray.length / 2,
                        tickInterval: 1,
                        label: "Time (hours ago)"
                    }
                },
                highlighter: {
                    show: true,
                    tooltipAxes: 'y'
                },
                series: [
                    {
                        color:"#6AB53C",
                        rendererOptions: {
                            smooth: false
                        }
                    }
                ]
            });
        });
    }
);

