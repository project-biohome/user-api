$(document).ready(function ()
{
	var data = [[[0, 0]]];

	$.jqplot.config.enablePlugins = true;

	var i = 0;
	while (i < 13)
	{
		data[0][i] = [i, 50];
		i++;
	}

	var chart = $.jqplot('line-chart-example',  data,
	{
		title:'Indice de confiance XD',
		animate: true,
		animateReplot: false,
		axes:{
			yaxis:
				{
				min:0, max:100,
				tickInterval: 10,
				},
			xaxis:
				{
				min:0, max: 12,
				tickInterval : 1,
				}
			},
		cursor:
			{
				show: true,
				style: 'crosshair',
				showTooltip: true,
				followMouse: true,
				tooltipAxisGroups: [['yaxis']],
				tooltipOffset: 15,
			},
		series:[{
			isDragable: true,
			dragable:{constrainTo : 'y'},
			color:"#6AB53C",
			rendererOptions :
				{
					//smooth: true,
					animation: {speed: 1500},
				},
			}],
	});
});