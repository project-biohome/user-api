from django.core.serializers.json import Serializer

class JSONSerializer(Serializer):

    def end_object(self, obj):
        concrete_model = obj._meta.concrete_model
        local_fields = concrete_model._meta.local_fields
        many_to_many = concrete_model._meta.many_to_many
        fields = concrete_model._meta.get_fields()
        fields = [x for x in [x for x in fields if x not in many_to_many] if x not in local_fields]
        for field in fields:
            if hasattr(field, 'attname'):
                if self.selected_fields is None or field.attname in self.selected_fields:
                    name = field.name
                    field = getattr(obj, field.name)
                    if hasattr(field, 'toJSON'):
                        self._current[name] = field.toJSON(self)
                    else:
                        self._current[name] = None
        super(JSONSerializer, self).end_object(obj)

    def value_from_field(self, obj, field):
        field = obj._meta.get_field(field)
        if field in obj._meta.concrete_model._meta.local_fields:
            if field.remote_field is None:
                return self.value_from_local_field(obj, field)
            else:
                return self.value_from_fk_field(obj, field)
        if field in obj._meta.concrete_model._meta.many_to_many:
            return self.value_from_m2m_field(obj, field)
        return None

    def value_from_local_field(self, obj, field):
        return field.value_from_object(obj)

    # not 100% sure this works in every situations
    def value_from_fk_field(self, obj, field):
        if self.use_natural_foreign_keys and hasattr(field.remote_field.model, 'natural_key'):
            related = getattr(obj, field.name)
            if related:
                value = related.natural_key()
            else:
                value = None
        else:
            value = self.value_from_local_field(obj, field)
        return value

    # not 100% sure this works in every situations
    def value_from_m2m_field(self, obj, field):
        if field.remote_field.through._meta.auto_created:
            if self.use_natural_foreign_keys and hasattr(field.remote_field.model, 'natural_key'):
                def m2m_value(value):
                    return value.natural_key()
            else:
                def m2m_value(value):
                    return self.value_from_local_field(value, value._meta.pk)
        return [
            m2m_value(related) for related in getattr(obj, field.name).iterator()
        ]

def errors_to_json(errors: dict):
    return {f: e.get_json_data() for f, e in errors.items()}