from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import validate_comma_separated_integer_list

from sheets.models import Sheet
from users.models import User


class ProgramCycleParam(models.Model):
    name = models.CharField(
        _('name'),
        blank=True,
        max_length=200,
        help_text=_("The parameter's name"),
    )

    values = models.CharField(
        _('values'),
        max_length=500,
        validators=[validate_comma_separated_integer_list],
        help_text=_('The 24 values of this parameter for the day, per hour'),
    )

    tolerance = models.DecimalField(
        _('tolerance'),
        max_digits=3,
        decimal_places=2,
        help_text=_('Tolerance of this parameter, in %'),
    )

    cycle = models.ForeignKey(
        'ProgramCycle',
        null=True,
        on_delete=models.CASCADE,
        help_text=_('The cycle this parameter belongs to'),
    )

    class Meta:
        ordering = ['name']
        verbose_name = _('cycle parameter')
        verbose_name_plural = _('cycle parameters')

    def __str__(self):
        return self.name
