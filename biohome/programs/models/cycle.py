from django.db import models
from django.utils.translation import ugettext_lazy as _

class ProgramCycle(models.Model):
    name = models.CharField(
        _('name'),
        max_length=50,
        help_text=_("The cycle's name"),
    )

    rounds = models.PositiveIntegerField(
        _('rounds'),
        help_text=_('The number of days within that cycle'),
    )

    program = models.ForeignKey(
        'Program',
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The program the cycle is attached to')
    )

    next = models.ForeignKey(
        'ProgramCycle',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_('The next cycle to be executed'),
    )

    def natural_key(self):
        return {
            'name': self.name,
            'rounds': self.rounds,
            'program': self.program.pk,
            'next': self.next.natural_key() if self.next else None,
            'id': self.pk,
        }

    class Meta:
        ordering = ['name']
        verbose_name = _('cycle')
        verbose_name_plural = _('cycles')

    def __str__(self):
        return self.name
