from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import validate_comma_separated_integer_list

from sheets.models import Sheet
from users.models import User


class ProgramReview(models.Model):
    title = models.CharField(
        _('title'),
        blank=True,
        max_length=80,
        help_text=_('The title of the review'),
    )

    review = models.TextField(
        _('review'),
        blank=True,
        help_text=_('The actual review'),
    )

    program = models.ForeignKey(
        'Program',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_('The reviewed program'),
    )

    yeld_weight = models.DecimalField(
        _('yeld weight'),
        max_digits=11,
        decimal_places=6,
        blank=True,
        default=0.0,
        help_text=_('The weight of the total yeld, in gr'),
    )

    yield_quantity = models.PositiveIntegerField(
        _('yeld quantity'),
        blank=True,
        default=0,
        help_text=_('The approximate number of individual flowers or leaves'),
    )

    yeld_size = models.DecimalField(
        _('yeld size'),
        max_digits=5,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_('The size of the harvested flowers or leaves, in cm'),
    )

    plant_size = models.DecimalField(
        _('plant size'),
        max_digits=5,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_('The size of the plant when harvested, in cm'),
    )

    yield_quality = models.DecimalField(
        _('yeld quality'),
        max_digits=2,
        decimal_places=0,
        blank=True,
        help_text=_("The yeld's quality, from 0 to 10"),
    )

    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The author of this review'),
    )

    username = models.CharField(
        _('username'),
        blank=True,
        max_length=80,
        help_text=_("The author's username"),
    )

    removed = models.BooleanField(
        _('is removed'),
        default=False,
        help_text=_('Should the review be hidden')
    )

    created_on = models.DateTimeField(
        _('created on'),
        auto_now_add=True,
        help_text=_('Date and time on which the review was created'),
    )

    updated_on = models.DateTimeField(
        _('updated on'),
        auto_now=True,
        help_text=_('Date and time on which the review was created'),
    )

    class Meta:
        get_latest_by = ['updated_on', 'created_on']
        ordering = ['updated_on', 'created_on', 'title']
        verbose_name = _('program review')
        verbose_name_plural = _('program reviews')

    def __str__(self):
        return self.title
