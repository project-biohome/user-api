from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import validate_comma_separated_integer_list

from sheets.models import Sheet
from users.models import User
from programs.models import ProgramWorkload, ProgramCycle

class Program(models.Model):
    name = models.CharField(
        _('name'),
        max_length=50,
        help_text=_("The program's name"),
    )

    sheet = models.ForeignKey(
        Sheet,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The associated sheet'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('Description of the program'),
    )

    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        related_name='created_programs',
        help_text=_('The author of the program'),
    )

    created_on = models.DateTimeField(
        _('created on'),
        auto_now_add=True,
        help_text=_('Date and time on which the program was created'),
     )

    updated_on = models.DateTimeField(
        _('updated on'),
        auto_now=True,
        help_text=_('Date and time on which the program was last updated'),
    )

    duration = models.DurationField(
        _('duration'),
        help_text=_('The total duration of the program')
    )

    yeld_weight = models.DecimalField(
        _('yeld weight'),
        max_digits=11,
        decimal_places=6,
        blank=True,
        default=0.0,
        help_text=_('The weight of the total yeld, in gr'),
    )

    yield_quantity = models.PositiveIntegerField(
        _('yeld quantity'),
        blank=True,
        default=0,
        help_text=_('The approximate number of individual flowers or leaves'),
    )

    yeld_size = models.DecimalField(
        _('yeld size'),
        max_digits=5,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_('The size of the harvested flowers or leaves, in cm'),
    )

    plant_size = models.DecimalField(
        _('plant size'),
        max_digits=5,
        decimal_places=2,
        blank=True,
        default=0,
        help_text=_('The size of the plant when harvested, in cm'),
    )

    yield_quality = models.DecimalField(
        _('yeld quality'),
        max_digits=2,
        decimal_places=0,
        help_text=_("The yeld's quality, from 0 to 10"),
    )

    workload = models.ForeignKey(
        ProgramWorkload,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The amount of intervention needed for this program'),
    )

    first_cycle = models.ForeignKey(
        ProgramCycle,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='program_first',
        help_text=_("The program's first cycle, it is not required"),
    )

    def natural_key(self):
        ret = {
            'name': self.name,
            'sheet': self.sheet.pk,
            'description': self.description,
            'author': self.author.username if self.author else None,
            'created_on': self.created_on,
            'updated_on': self.created_on,
            'duration': self.duration,
            'yield_weight': float(self.yeld_weight),
            'yield_quantity': self.yield_quantity,
            'yield_size': float(self.yeld_size),
            'plant_size': float(self.plant_size),
            'yield_quality': float(self.yield_quality),
            'workload': self.workload.pk,
            'first_cycle': None,
            'id': self.pk,
        }
        if self.first_cycle:
            ret['first_cycle'] = self.first_cycle.pk
        return ret

    class Meta:
        get_latest_by = 'updated_on'
        ordering = ['name']
        verbose_name = _('program')
        verbose_name_plural = _('programs')

    def __str__(self):
        return self.name
