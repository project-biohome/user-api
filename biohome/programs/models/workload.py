from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import validate_comma_separated_integer_list

from sheets.models import Sheet
from users.models import User


class ProgramWorkload(models.Model):
    workload = models.CharField(
        _('workload'),
        max_length=50,
        default='medium',
        help_text=_('The workload'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of this workload'),
    )

    def natural_key(self):
        return {
            'workload': self.workload,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        verbose_name = 'workload'
        verbose_name_plural = 'workloads'

    def __str__(self):
        return self.workload
