from django.conf.urls import url, include
from django.views.generic import RedirectView

from programs.views import (
    ProgramList,
    ProgramCreate,
    ProgramUpdate,
    ProgramDetail,
    ProgramDelete,

    ProgramCycleList,
    ProgramCycleCreate,
    ProgramCycleUpdate,
    ProgramCycleDetail,
    ProgramCycleDelete,

    ProgramCycleParamList,
    ProgramCycleParamCreate,
    ProgramCycleParamUpdate,
    ProgramCycleParamDetail,
    ProgramCycleParamDelete,

    ProgramReviewList,
    ProgramReviewCreate,
    ProgramReviewUpdate,
    ProgramReviewDetail,
    ProgramReviewDelete,

    ProgramWorkloadList,
    ProgramWorkloadCreate,
    ProgramWorkloadUpdate,
    ProgramWorkloadDetail,
    ProgramWorkloadDelete,
)

program_patterns = [
    url(r'^$', ProgramList.as_view(), name='program_list'),
    url(r'^add$', ProgramCreate.as_view(), name='program_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="program_detail")),
    url(r'^(?P<pk>\d+)/update$', ProgramUpdate.as_view(), name='program_update'),
    url(r'^(?P<pk>\d+)/details$', ProgramDetail.as_view(), name='program_detail'),
    url(r'^(?P<pk>\d+)/delete$', ProgramDelete.as_view(), name='program1_delete'),
]

cycle_patterns = [
    url(r'^$', ProgramCycleList.as_view(), name='cycle_list'),
    url(r'^add$', ProgramCycleCreate.as_view(), name='cycle_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="cycle_detail")),
    url(r'^(?P<pk>\d+)/update$', ProgramCycleUpdate.as_view(), name='cycle_update'),
    url(r'^(?P<pk>\d+)/details$', ProgramCycleDetail.as_view(), name='cycle_detail'),
    url(r'^(?P<pk>\d+)/delete$', ProgramCycleDelete.as_view(), name='cycle1_delete'),
]

cycleparam_patterns = [
    url(r'^$', ProgramCycleParamList.as_view(), name='cycleparam_list'),
    url(r'^add$', ProgramCycleParamCreate.as_view(), name='cycleparam_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="cycleparam_detail")),
    url(r'^(?P<pk>\d+)/update$', ProgramCycleParamUpdate.as_view(), name='cycleparam_update'),
    url(r'^(?P<pk>\d+)/details$', ProgramCycleParamDetail.as_view(), name='cycleparam_detail'),
    url(r'^(?P<pk>\d+)/delete$', ProgramCycleParamDelete.as_view(), name='cycleparam1_delete'),
]

review_patterns = [
    url(r'^$', ProgramReviewList.as_view(), name='review_list'),
    url(r'^add$', ProgramReviewCreate.as_view(), name='review_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="review_detail")),
    url(r'^(?P<pk>\d+)/update$', ProgramReviewUpdate.as_view(), name='review_update'),
    url(r'^(?P<pk>\d+)/details$', ProgramReviewDetail.as_view(), name='review_detail'),
    url(r'^(?P<pk>\d+)/delete$', ProgramReviewDelete.as_view(), name='review1_delete'),
]

workload_patterns = [
    url(r'^$', ProgramWorkloadList.as_view(), name='workload_list'),
    url(r'^add$', ProgramWorkloadCreate.as_view(), name='workload_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="workload_detail")),
    url(r'^(?P<pk>\d+)/update$', ProgramWorkloadUpdate.as_view(), name='workload_update'),
    url(r'^(?P<pk>\d+)/details$', ProgramWorkloadDetail.as_view(), name='workload_detail'),
    url(r'^(?P<pk>\d+)/delete$', ProgramWorkloadDelete.as_view(), name='workload1_delete'),
]

urlpatterns = [
    url(r'^', include(program_patterns)),
    url(r'^cycle/', include(cycle_patterns)),
    url(r'^cycle-param/', include(cycleparam_patterns)),
    url(r'^review/', include(review_patterns)),
    url(r'^workload/', include(workload_patterns)),
]
