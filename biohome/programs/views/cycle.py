from django.urls import reverse_lazy
from django.views.generic import DeleteView

from programs.models import ProgramCycle
from programs.forms import ProgramCycleForm

from programs.views import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)

from users.mixins import BiohomePermissionMixin

class ProgramCycleView:
    model = ProgramCycle
    form_class = ProgramCycleForm
    success_url = reverse_lazy('cycle_list')

class ProgramCycleList(ProgramCycleView, GenericProgramListView):
    pass

class ProgramCycleCreate(BiohomePermissionMixin, ProgramCycleView, GenericProgramCreateView):
    permission_required = 'programs.cycle.can_add'

    pass

class ProgramCycleUpdate(BiohomePermissionMixin, ProgramCycleView, GenericProgramUpdateView):
    permission_required = 'programs.cycle.can_edit'

    pass

class ProgramCycleDetail(ProgramCycleView, GenericProgramDetailView):
    pass

class ProgramCycleDelete(BiohomePermissionMixin, ProgramCycleView, GenericProgramView, DeleteView):
    permission_required = 'programs.cycle.can_delete'

    pass
