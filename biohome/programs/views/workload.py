from django.urls import reverse_lazy
from django.views.generic import DeleteView

from programs.models import ProgramWorkload
from programs.forms import ProgramWorkloadForm

from programs.views import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)

from users.mixins import BiohomePermissionMixin

class ProgramWorkloadView:
    model = ProgramWorkload
    form_class = ProgramWorkloadForm
    success_url = reverse_lazy('workload_list')

class ProgramWorkloadList(ProgramWorkloadView, GenericProgramListView):
    pass

class ProgramWorkloadCreate(BiohomePermissionMixin, ProgramWorkloadView, GenericProgramCreateView):
    permission_required = 'programs.workload.can_add'

    pass

class ProgramWorkloadUpdate(BiohomePermissionMixin, ProgramWorkloadView, GenericProgramUpdateView):
    permission_required = 'programs.workload.can_edit'

    pass

class ProgramWorkloadDetail(ProgramWorkloadView, GenericProgramDetailView):
    pass

class ProgramWorkloadDelete(BiohomePermissionMixin, ProgramWorkloadView, GenericProgramView, DeleteView):
    permission_required = 'programs.workload.can_delete'

    pass
