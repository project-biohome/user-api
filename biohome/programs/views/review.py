from django.urls import reverse_lazy
from django.views.generic import DeleteView

from programs.models import ProgramReview
from programs.forms import ProgramReviewForm

from programs.views import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)


class ProgramReviewView:
    model = ProgramReview
    form_class = ProgramReviewForm
    success_url = reverse_lazy('review_list')

class ProgramReviewList(ProgramReviewView, GenericProgramListView):
    pass

class ProgramReviewCreate(ProgramReviewView, GenericProgramCreateView):
    pass

class ProgramReviewUpdate(ProgramReviewView, GenericProgramUpdateView):
    pass

class ProgramReviewDetail(ProgramReviewView, GenericProgramDetailView):
    pass

class ProgramReviewDelete(ProgramReviewView, GenericProgramView, DeleteView):
    pass
