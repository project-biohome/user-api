from django.urls import reverse_lazy
from django.views.generic import DeleteView

from programs.models import ProgramCycleParam
from programs.forms import ProgramCycleParamForm

from programs.views import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)

from users.mixins import BiohomePermissionMixin

class ProgramCycleParamView:
    model = ProgramCycleParam
    form_class = ProgramCycleParamForm
    success_url = reverse_lazy('cycleparam_list')

class ProgramCycleParamList(ProgramCycleParamView, GenericProgramListView):
    pass

class ProgramCycleParamCreate(BiohomePermissionMixin, ProgramCycleParamView, GenericProgramCreateView):
    permission_required = 'programs.cycleparam.can_add'

    pass

class ProgramCycleParamUpdate(BiohomePermissionMixin, ProgramCycleParamView, GenericProgramUpdateView):
    permission_required = 'programs.cycleparam.can_edit'

    pass

class ProgramCycleParamDetail(ProgramCycleParamView, GenericProgramDetailView):
    pass

class ProgramCycleParamDelete(BiohomePermissionMixin, ProgramCycleParamView, GenericProgramView, DeleteView):
    permission_required = 'programs.cycleparam.can_delete'

    pass
