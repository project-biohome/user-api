from .genericprogramviews import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)

from .program import (
    ProgramList,
    ProgramCreate,
    ProgramUpdate,
    ProgramDetail,
    ProgramDelete,
)

from .cycle import (
    ProgramCycleList,
    ProgramCycleCreate,
    ProgramCycleUpdate,
    ProgramCycleDetail,
    ProgramCycleDelete,
)

from .cycleparam import (
    ProgramCycleParamList,
    ProgramCycleParamCreate,
    ProgramCycleParamUpdate,
    ProgramCycleParamDetail,
    ProgramCycleParamDelete,
)

from .review import (
    ProgramReviewList,
    ProgramReviewCreate,
    ProgramReviewUpdate,
    ProgramReviewDetail,
    ProgramReviewDelete,
)

from .workload import (
    ProgramWorkloadList,
    ProgramWorkloadCreate,
    ProgramWorkloadUpdate,
    ProgramWorkloadDetail,
    ProgramWorkloadDelete,
)
