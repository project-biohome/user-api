from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from programs.models import Program
from programs.forms import ProgramForm

from programs.views import (
    GenericProgramView,
    GenericProgramListView,
    GenericProgramCreateView,
    GenericProgramUpdateView,
    GenericProgramDetailView,
)

from users.mixins import BiohomePermissionMixin


class ProgramView(LoginRequiredMixin):
    model = Program
    form_class = ProgramForm
    success_url = reverse_lazy('program_list')

class ProgramList(ProgramView, GenericProgramListView):
    pass

class ProgramCreate(BiohomePermissionMixin, ProgramView, GenericProgramCreateView):
    permission_required = 'programs.program.can_add'

    pass

class ProgramUpdate(BiohomePermissionMixin, ProgramView, GenericProgramUpdateView):
    permission_required = 'programs.program.can_edit'

    pass

class ProgramDetail(ProgramView, GenericProgramDetailView):
    def get_context_data(self, **kwargs):
        context = super(GenericProgramDetailView, self).get_context_data()
        from biomes.models.events.culture import CULTURE_START
        context['CULTURE_START'] = CULTURE_START
        return context

class ProgramDelete(BiohomePermissionMixin, ProgramView, GenericProgramView, DeleteView):
    permission_required = 'programs.program.can_delete'

    pass
