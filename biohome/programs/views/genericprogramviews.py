from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
)
from django.core import serializers
from django.http import JsonResponse

from json_serializer import JSONSerializer

import json


class GenericProgramView:
    def get_template_names(self):
        if self.template_name:
            return self.template_name
        if hasattr(self, 'object_list'):
            opts = self.object_list.model._meta
        else:
            opts = self.model._meta
        return ('%s/%s%s.html' % (opts.model_name, opts.model_name, self.template_name_suffix))

class GenericProgramListView(GenericProgramView, ListView):
    def get(self, request):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields
            result = JSONSerializer().serialize(self.model.objects.all(), fields=fields)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(ListView, self).get(request)

class GenericProgramCreateView(GenericProgramView, CreateView):
    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            form = self.get_form()
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': form.errors.as_text()}, status=400)
        return super().post(request, *args, **kwargs)

class GenericProgramUpdateView(GenericProgramView, UpdateView):
    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            obj = self.model.objects.get(pk=kwargs['pk'])
            form = self.form_class(request.POST, instance=obj)
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': form.errors.as_text()}, status=400)
        return super().post(request, *args, **kwargs)

class GenericProgramDetailView(GenericProgramView, DetailView):
    def get(self, request, pk):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields
            result = JSONSerializer().serialize([self.model.objects.get(pk=pk)], fields=fields, use_natural_foreign_keys=True)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(DetailView, self).get(request)
