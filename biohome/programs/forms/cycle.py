from django import forms

from programs.models import ProgramCycle
from widgets import widgets

class ProgramCycleForm(forms.ModelForm):
    class Meta:
        model = ProgramCycle
        fields = [
            'name',
            'rounds',
            'program',
            'next'
        ]

        widgets = {
            'name': widgets.TextInput,
            'rounds': widgets.TextInput,
            'program': widgets.Select,
            'next': widgets.Select,
        }
