from django import forms

from programs.models import ProgramReview


class ProgramReviewForm(forms.ModelForm):
    class Meta:
        model = ProgramReview
        fields = [
            'title',
            'review',
            'program',
            'yeld_weight',
            'yield_quantity',
            'yeld_size',
            'plant_size',
            'yield_quality',
            'author',
            'username',
            'removed',
        ]
