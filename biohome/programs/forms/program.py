from django import forms

from programs.models import Program
from widgets import widgets

class ProgramForm(forms.ModelForm):
    class Meta:
        model = Program
        fields = [
            'name',
            'sheet',
            'description',
            'author',
            'duration',
            'yeld_weight',
            'yield_quantity',
            'yeld_size',
            'plant_size',
            'yield_quality',
            'workload',
            'first_cycle',
        ]

        widgets = {
            'name': widgets.TextInput,
            'sheet': widgets.Select,
            'description': widgets.Textarea,
            'author': widgets.Select,
            'duration': widgets.TextInput,
            'yeld_weight': widgets.TextInput,
            'yield_quantity': widgets.TextInput,
            'yeld_size': widgets.TextInput,
            'plant_size': widgets.TextInput,
            'yield_quality': widgets.TextInput,
            'workload': widgets.Select,
            'first_cycle': widgets.Select,
        }