from django import forms

from programs.models import ProgramCycleParam


class ProgramCycleParamForm(forms.ModelForm):
    class Meta:
        model = ProgramCycleParam
        fields = [
            'name',
            'values',
            'tolerance',
            'cycle',
        ]
