from django import forms

from programs.models import ProgramWorkload
from widgets import widgets

class ProgramWorkloadForm(forms.ModelForm):
    class Meta:
        model = ProgramWorkload
        fields = [
            'workload',
            'description',
        ]

        widgets = {
            'workload': widgets.TextInput,
            'description': widgets.Textarea,
        }
