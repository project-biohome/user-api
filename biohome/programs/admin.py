from django.contrib import admin

from .models import (ProgramReview, ProgramWorkload, Program, ProgramCycle,
    ProgramCycleParam)


@admin.register(ProgramReview)
class ProgramReviewAdmin(admin.ModelAdmin):
    list_display = ('title', 'username', 'removed', 'updated_on')

@admin.register(ProgramCycleParam)
class ProgramCycleParamAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(ProgramCycle)
class ProgramCycleAdmin(admin.ModelAdmin):
    list_display = ('name', 'rounds', 'program', 'next')

@admin.register(ProgramWorkload)
class ProgramWorkloadAdmin(admin.ModelAdmin):
    list_display = ('workload',)

@admin.register(Program)
class ProgramAdmin(admin.ModelAdmin):
    list_display = ('name', 'sheet')
