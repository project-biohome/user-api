from django.contrib.contenttypes.fields import GenericRelation, ReverseGenericManyToOneDescriptor

from votes.models import Vote


class ReverseVoteDescriptor(ReverseGenericManyToOneDescriptor):

    def __init__(self, rel):
        super(ReverseVoteDescriptor, self).__init__(rel)

    def __get__(self, instance, cls=None):
        manager = super(ReverseVoteDescriptor, self).__get__(instance, cls)

        class VoteManager(manager.__class__):

            def vote(self, value, user, reset=False):
                vote = self.instance.votes.filter(user=user)
                if vote.count():
                    if reset:
                        vote.delete()
                    else:
                        vote = vote[0]
                        vote.value = value
                        vote.save()
                else:
                    self.instance.votes.create(value=value, user=user)

            def upvote(self, user):
                self.vote(True, user)

            def downvote(self, user):
                self.vote(False, user)

            def upvote_or_reset(self, user):
                self.vote(True, user, True)

            def downvote_or_reset(self, user):
                self.vote(False, user, True)

            def get(self, user):
                vote = self.instance.votes.filter(user=user)
                if vote.count():
                    return vote.value
                return None

            def nb_upvote(self):
                return instance.votes.filter(value=True).count()

            def nb_downvote(self):
                return instance.votes.filter(value=False).count()

            def score(self):
                return self.nb_upvote() - self.nb_downvote()

            def toJSON(self, serializer):
                return {
                    'upvote': self.nb_upvote(),
                    'downvote': self.nb_downvote(),
                }

        manager = VoteManager(manager.instance)
        return manager


class VoteField(GenericRelation):

    def __init__(self, **kwargs):
        super(VoteField, self).__init__(Vote, **kwargs)

    def contribute_to_class(self, cls, name):
        super(VoteField, self).contribute_to_class(cls, name)
        setattr(cls, name, ReverseVoteDescriptor(self.remote_field))
