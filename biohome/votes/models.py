from django.db import models
from django.utils.translation import ugettext as __
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from users.models import User

class Vote(models.Model):
    value = models.NullBooleanField(
        _('value'),
    )

    # ContentTypeField
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    user = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The user who voted'),
    )

    created_on = models.DateTimeField(
        _('created on'),
        auto_now_add=True,
        help_text=_('Date and time on which the vote was created'),
    )

    updated_on = models.DateTimeField(
        _('updated on'),
        auto_now=True,
        help_text=_('Date and time on which the vote was updated'),
    )

    class Meta:
        get_latest_by = ['updated_on', 'created_on']
        ordering = ['updated_on', 'created_on']
        verbose_name = _('vote'),
        verbose_name_plural = _('vote')

    def __str__(self):
        if self.value == True:
            return __('up')
        elif self.value == False:
            return __('down')
        return __('none')
