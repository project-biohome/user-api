from django.contrib import admin
from django.db import models

from .models import Vote

@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('value', 'user', 'created_on', 'updated_on')
