from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from users.models import User

class Comment(models.Model):
    title = models.CharField(
        _('title'),
        blank=True,
        max_length=80,
        help_text=_('The title of the comment'),
    )

    comment = models.TextField(
        _('comment'),
        blank=True,
        help_text=_('The actual comment'),
    )

    # ContentTypeField
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    author = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The author of this review'),
    )

    username = models.CharField(
        _('username'),
        blank=True,
        max_length=80,
        help_text=_("The author's username"),
    )

    removed = models.BooleanField(
        _('is removed'),
        default=False,
        help_text=_('Should the review be hidden')
    )

    created_on = models.DateTimeField(
        _('created on'),
        auto_now_add=True,
        help_text=_('Date and time on which the comment was created'),
    )

    updated_on = models.DateTimeField(
        _('updated on'),
        auto_now=True,
        help_text=_('Date and time on which the comment was created'),
    )

    class Meta:
        get_latest_by = ['updated_on', 'created_on']
        ordering = ['updated_on', 'created_on', 'title']
        verbose_name = _('comment'),
        verbose_name_plural = _('comments')

    def __str__(self):
        return self.title
