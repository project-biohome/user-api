from django.contrib import admin
from django.db import models

from .models import Comment

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('title', 'username', 'updated_on', 'removed')
