from django.contrib.contenttypes.fields import GenericRelation, ReverseGenericManyToOneDescriptor

from comments.models import Comment


class ReverseCommentDescriptor(ReverseGenericManyToOneDescriptor):

    def __init__(self, rel):
        super(ReverseCommentDescriptor, self).__init__(rel)

    def __get__(self, instance, cls=None):
        manager = super(ReverseCommentDescriptor, self).__get__(instance, cls)

        class CommentManager(manager.__class__):

            def toJSON(self, serializer):
                json = []
                for comment in self.instance.comments.all():
                    json.append({
                        'title': comment.title,
                        'comment': comment.comment,
                        'author': serializer.value_from_field(comment, 'author'),
                        'username': comment.username,
                        'id': comment.pk,
                    })
                return json

        manager = CommentManager(manager.instance)
        return manager


class CommentField(GenericRelation):

    def __init__(self, **kwargs):
        super(CommentField, self).__init__(Comment, **kwargs)

    def contribute_to_class(self, cls, name):
        super(CommentField, self).contribute_to_class(cls, name)
        setattr(cls, name, ReverseCommentDescriptor(self.remote_field))
