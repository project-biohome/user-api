try:
    from biohome.shared_settings import *
except ImportError:
    pass

from django.utils.translation import ugettext_lazy as _

# General
DEBUG = True
INTERNAL_IPS = ['127.0.0.1']
SECRET_KEY = 'mqo)+q#f$%1h&*ppu_j&380ufhi!qqi3g1rqhx5@e-tvmpjx4)'

# Media
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = 'media/'

# Static
STATIC_URL = '/static/'
STATICFILES_DIRS += [
    os.path.join(BASE_DIR, "static")
]

# CSRF
CSRF_USE_SESSIONS = True

# Email
DEFAULT_FROM_EMAIL = 'dev-django@biohome.fr'
SERVER_EMAIL = 'dev-django@biohome.fr'

EMAIL_HOST = 'mail.gandi.net'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'dev-django@biohome.fr'
EMAIL_HOST_PASSWORD = 'biohomeganditopkek'
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Sessions
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_NAME = 'biohome_session'
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# Language
LANGUAGE_CODE = 'fr-fr'
LANGUAGE_COOKIE_AGE = 60 * 60 * 24 * 30
LANGUAGE_COOKIE_NAME = 'biohome_language'
LANGUAGES = [
  ('en', _('English')),
  ('fr', _('Français')),
]
LOCALE_PATHS = [
    'locale',
]

# Apps
INSTALLED_APPS += [
    'debug_toolbar',
    'users',
    'comments',
    'votes',
    'sheets',
    'programs',
    'biomes',
    'dashboard',
    'widgets',

    'sheets.templatetags',
]

# Middlewares
MIDDLEWARE = [
    'debug_toolbar.middleware.DebugToolbarMiddleware',
] + MIDDLEWARE

# Databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Logs
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': os.path.join(BASE_DIR, 'debug.log'),
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}
