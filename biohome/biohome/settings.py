try:
    from biohome.shared_settings import *
except ImportError:
    pass
import os

from django.utils.translation import ugettext_lazy as _

# General
DEBUG=False
INTERNAL_IPS = ['37.59.60.192']
ADMINS = [('Biohome', 'django@biohome.fr')]
SECRET_KEY = os.environ['SECRET_KEY']
ALLOWED_HOSTS = ['api.biohome.fr', 'biome.biohome.fr']

# CSRF
CSRF_COOKIE_AGE = None
CSRF_COOKIE_DOMAIN = '.biohome.fr'
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
CSRF_USE_SESSIONS = True
CSRF_TRUSTED_ORIGINS = ['api.biohome.fr', 'biome.biohome.fr']

# Databases
DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.postgresql',
       'NAME': os.environ['DB_NAME'],
       'USER': os.environ['DB_USER'],
       'PASSWORD': os.environ['DB_PASSWORD'],
       'HOST': os.environ['DB_HOST'],
       'PORT': os.environ['DB_PORT'],
   }
}

# Data Upload
DATA_UPLOAD_MAX_MEMORY_SIZE = 102400
DATA_UPLOAD_MAX_NUMBER_FIELDS = 150

# File Upload
FILE_UPLOAD_MAX_MEMORY_SIZE = 2621440
FILE_UPLOAD_DIRECTORY_PERMISSIONS = 0o644
FILE_UPLOAD_PERMISSIONS = 0o644
FILE_UPLOAD_TEMP_DIR = '/tmp'

# Email
DEFAULT_FROM_EMAIL = 'contact@biohome.fr'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'mail.gandi.net'
EMAIL_HOST_PASSWORD = os.environ['EMAIL_PASSWORD']
EMAIL_HOST_USER = 'django@biohome.fr'
EMAIL_PORT = 465
EMAIL_SUBJECT_PREFIX = '[Django] '
EMAIL_USE_SSL = True
EMAIL_TIMEOUT = 5
SERVER_EMAIL = 'django@biohome.fr'

# Apps
INSTALLED_APPS += [
    'users',
    'comments',
    'votes',
    'sheets',
    'programs',
    'biomes',
    'dashboard',
    'widgets',

    'sheets.templatetags',
]

# Language
LANGUAGE_CODE = 'fr-fr'
LANGUAGE_COOKIE_AGE = 60 * 60 * 24 * 30
LANGUAGE_COOKIE_DOMAIN = ['.biohome.fr']
LANGUAGE_COOKIE_NAME = 'biohome_language'
LANGUAGES = [
  ('en', _('English')),
  ('fr', _('Français')),
]
LOCALE_PATHS = [
    'locale',
]

# Media
MEDIA_ROOT = '/media'
MEDIA_URL = 'media/'

# Security
SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_SECONDS = 3600
# SECURE_SSL_REDIRECT = True
X_FRAME_OPTIONS = 'DENY'

# Static
STATIC_ROOT = '/static'
STATIC_URL = 'https://api.biohome.fr/static/'
STATICFILES_DIRS += [
    '/code/static',
]

# Authentication
PASSWORD_RESET_TIMEOUT_DAYS = 1
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        'OPTIONS': {
            'min_length': 8,
        }
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Messages
from django.contrib import messages
MESSAGE_LEVEL = messages.DEBUG

# Sessions
SESSION_COOKIE_AGE = 60 * 60 * 24 * 7
SESSION_COOKIE_DOMAIN = '.biohome.fr'
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_NAME = 'biohome_session'
SESSION_COOKIE_SECURE = True
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_SERIALIZER = 'django.contrib.sessions.serializers.JSONSerializer'

# Logs
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/debug.log',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}
