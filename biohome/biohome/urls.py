"""biohome URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.views.defaults import page_not_found, permission_denied
from django.views.static import serve
from machina.app import board

urlpatterns = [
    url(r'^404/$', page_not_found, kwargs={'exception': Exception("Page not found")}),
    url(r'^403/$', permission_denied, kwargs={'exception': Exception("Permission denied")}),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('users.urls')),
    url(r'^', include('dashboard.urls')),
    url(r'^sheets/', include('sheets.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^programs/', include('programs.urls')),
    url(r'^biomes/', include('biomes.urls')),
    url(r'^forum/', include(board.urls)),
    url(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT}),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
