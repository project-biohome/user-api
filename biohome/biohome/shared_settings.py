import os
from django.utils.translation import ugettext_lazy as _
from machina import (
    get_apps as get_machina_apps,
    MACHINA_MAIN_STATIC_DIR,
    MACHINA_MAIN_TEMPLATE_DIR,
)

# Authentication
AUTH_USER_MODEL = 'users.User'
LOGIN_URL = '/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
USER_GROUP_NAME = 'Biohomie'

# Static
STATICFILES_DIRS = [
    MACHINA_MAIN_STATIC_DIR,
]

# General
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_URLCONF = 'biohome.urls'
TIME_ZONE = 'UTC'

# Misc
APPEND_SLASH = True
FIRST_DAY_OF_WEEK = 1

# Internationalization
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Apps
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Machina related apps
    'mptt',
    'haystack',
    'widget_tweaks',
] + get_machina_apps(['apps.forum', 'apps.forum_conversation'])

# Middlewaress
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'biohome.middleware.DisableCsrfCheck',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # Machina
    'machina.apps.forum_permission.middleware.ForumPermissionMiddleware',
]

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            os.path.join(BASE_DIR, 'templates/machina'),
            MACHINA_MAIN_TEMPLATE_DIR,
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # Machina
                'machina.core.context_processors.metadata',
            ],
        },
    },
]

# Caches
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'machina_attachments': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp',
    },
}

# Haystack
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}

# Migrations
MIGRATION_MODULES = {
    'forum': 'machina.apps.forum.migrations',
    'forum_conversation': 'machina.apps.forum_conversation.migrations',
}

WSGI_APPLICATION = 'biohome.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'
