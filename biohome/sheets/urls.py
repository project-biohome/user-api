from django.conf.urls import url, include
from django.views.generic import RedirectView

from sheets.views import (
    SheetCareList, SheetCareCreate, SheetCareUpdate, SheetCareDetail, SheetCareDelete,
    SheetClimateList, SheetClimateCreate, SheetClimateUpdate, SheetClimateDetail, SheetClimateDelete,
    SheetExpositionList, SheetExpositionCreate, SheetExpositionUpdate, SheetExpositionDetail, SheetExpositionDelete,
    SheetRegionList, SheetRegionCreate, SheetRegionUpdate, SheetRegionDetail, SheetRegionDelete,
    SheetList, SheetCreate, SheetUpdate, SheetDetail, SheetDelete, SheetUpvote, SheetDownvote, SheetComment, SheetSearch,
    SheetSizeList, SheetSizeCreate, SheetSizeUpdate, SheetSizeDetail, SheetSizeDelete,
    SheetTaxonomyList, SheetTaxonomyCreate, SheetTaxonomyUpdate, SheetTaxonomyDetail, SheetTaxonomyDelete,
    SheetTaxonomyRankList, SheetTaxonomyRankCreate, SheetTaxonomyRankUpdate, SheetTaxonomyRankDetail, SheetTaxonomyRankDelete,
    SheetTagList, SheetTagCreate, SheetTagUpdate, SheetTagDetail, SheetTagDelete,
)

care_patterns = [
    url(r'^$', SheetCareList.as_view(), name='sheet_care_list'),
    url(r'^add$', SheetCareCreate.as_view(), name='sheet_care_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_care_detail")),
    url(r'^(?P<pk>\d+)/details$', SheetCareDetail.as_view(), name='sheet_care_detail'),
    url(r'^(?P<pk>\d+)/update$', SheetCareUpdate.as_view(), name='sheet_care_update'),
    url(r'^(?P<pk>\d+)/details$', SheetCareDetail.as_view(), name='sheet_care_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetCareDelete.as_view(), name='sheet_care_delete'),
]

climate_patterns = [
    url(r'^$', SheetClimateList.as_view(), name='sheet_climate_list'),
    url(r'^add$', SheetClimateCreate.as_view(), name='sheet_climate_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_climate_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetClimateUpdate.as_view(), name='sheet_climate_update'),
    url(r'^(?P<pk>\d+)/details$', SheetClimateDetail.as_view(), name='sheet_climate_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetClimateDelete.as_view(), name='sheet_climate_delete'),
]

exposition_patterns = [
    url(r'^$', SheetExpositionList.as_view(), name='sheet_exposition_list'),
    url(r'^add$', SheetExpositionCreate.as_view(), name='sheet_exposition_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_exposition_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetExpositionUpdate.as_view(), name='sheet_exposition_update'),
    url(r'^(?P<pk>\d+)/details$', SheetExpositionDetail.as_view(), name='sheet_exposition_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetExpositionDelete.as_view(), name='sheet_exposition_delete'),
]

region_patterns = [
    url(r'^$', SheetRegionList.as_view(), name='sheet_region_list'),
    url(r'^add$', SheetRegionCreate.as_view(), name='sheet_region_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_region_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetRegionUpdate.as_view(), name='sheet_region_update'),
    url(r'^(?P<pk>\d+)/details$', SheetRegionDetail.as_view(), name='sheet_region_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetRegionDelete.as_view(), name='sheet_region_delete'),
]

sheet_patterns = [
    url(r'^$', SheetSearch.as_view(), name='sheet_search'),
    url(r'^list$', SheetList.as_view(), name='sheet_list'),
    url(r'^add$', SheetCreate.as_view(), name='sheet_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetUpdate.as_view(), name='sheet_update'),
    url(r'^(?P<pk>\d+)/details$', SheetDetail.as_view(), name='sheet_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetDelete.as_view(), name='sheet_delete'),
    url(r'^(?P<pk>\d+)/upvote$', SheetUpvote.as_view(), name='sheet_upvote'),
    url(r'^(?P<pk>\d+)/downvote$', SheetDownvote.as_view(), name='sheet_downvote'),
    url(r'^(?P<pk>\d+)/comment$', SheetComment.as_view(), name='sheet_comment'),
]

size_patterns = [
    url(r'^$', SheetSizeList.as_view(), name='sheet_size_list'),
    url(r'^add$', SheetSizeCreate.as_view(), name='sheet_size_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_size_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetSizeUpdate.as_view(), name='sheet_size_update'),
    url(r'^(?P<pk>\d+)/details$', SheetSizeDetail.as_view(), name='sheet_size_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetSizeDelete.as_view(), name='sheet_size_delete'),
]

taxonomy_rank_patterns = [
    url(r'^$', SheetTaxonomyRankList.as_view(), name='sheet_taxonomy_rank_list'),
    url(r'^add$', SheetTaxonomyRankCreate.as_view(), name='sheet_taxonomy_rank_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_taxonomy_rank_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetTaxonomyRankUpdate.as_view(), name='sheet_taxonomy_rank_update'),
    url(r'^(?P<pk>\d+)/details$', SheetTaxonomyRankDetail.as_view(), name='sheet_taxonomy_rank_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetTaxonomyRankDelete.as_view(), name='sheet_taxonomy_rank_delete'),
]

taxonomy_patterns = [
    url(r'^$', SheetTaxonomyList.as_view(), name='sheet_taxonomy_list'),
    url(r'^add$', SheetTaxonomyCreate.as_view(), name='sheet_taxonomy_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_taxonomy_detail")),
    url(r'^(?P<pk>\d+)/update$', SheetTaxonomyUpdate.as_view(), name='sheet_taxonomy_update'),
    url(r'^(?P<pk>\d+)/details$', SheetTaxonomyDetail.as_view(), name='sheet_taxonomy_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetTaxonomyDelete.as_view(), name='sheet_taxonomy_delete'),
]

tag_patterns = [
    url(r'^$', SheetTagList.as_view(), name='sheet_tag_list'),
    url(r'^add$', SheetTagCreate.as_view(), name='sheet_tag_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="sheet_tag_detail")),
    url(r'^(?P<pk>\d+)/details$', SheetTagDetail.as_view(), name='sheet_tag_detail'),
    url(r'^(?P<pk>\d+)/update$', SheetTagUpdate.as_view(), name='sheet_tag_update'),
    url(r'^(?P<pk>\d+)/details$', SheetTagDetail.as_view(), name='sheet_tag_detail'),
    url(r'^(?P<pk>\d+)/delete$', SheetTagDelete.as_view(), name='sheet_tag_delete'),
]

urlpatterns = [
    url(r'^', include(sheet_patterns)),
    url(r'^care/', include(care_patterns)),
    url(r'^climate/', include(climate_patterns)),
    url(r'^exposition/', include(exposition_patterns)),
    url(r'^region/', include(region_patterns)),
    url(r'^size/', include(size_patterns)),
    url(r'^taxonomy-rank/', include(taxonomy_rank_patterns)),
    url(r'^taxonomy/', include(taxonomy_patterns)),
    url(r'^tag/', include(tag_patterns)),
]
