from django.contrib import admin
from django.db import models

from .widgets import AdminImageWidget
from .models import (
    Sheet,
    SheetCare,
    SheetClimate,
    SheetSize,
    SheetExposition,
    SheetTaxonomy,
    SheetTaxonomyRank,
    SheetRegion,
    SheetTag,
)


@admin.register(Sheet)
class SheetAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_by', 'updated_on', 'created_on')
    image_fields = ['image']

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name in self.image_fields:
            request = kwargs.pop("request", None)
            kwargs['widget'] = AdminImageWidget
            return db_field.formfield(**kwargs)
        return super(SheetAdmin, self).formfield_for_dbfield(db_field, **kwargs)


@admin.register(SheetCare)
class SheetCareAdmin(admin.ModelAdmin):
    list_display = ('care',)

@admin.register(SheetClimate)
class SheetClimateAdmin(admin.ModelAdmin):
    list_display = ('climate',)

@admin.register(SheetSize)
class SheetSizeAdmin(admin.ModelAdmin):
    list_display = ('size',)

@admin.register(SheetExposition)
class SheetExpositionAdmin(admin.ModelAdmin):
    list_display = ('exposition',)

@admin.register(SheetTaxonomyRank)
class SheetTaxonomyRankAdmin(admin.ModelAdmin):
    list_display = ('rank',)

@admin.register(SheetTaxonomy)
class SheetTaxonomyAdmin(admin.ModelAdmin):
    list_display = ('name',)

@admin.register(SheetRegion)
class SheetRegionAdmin(admin.ModelAdmin):
    list_display = ('region',)

@admin.register(SheetTag)
class SheetTagAdmin(admin.ModelAdmin):
    list_display = ('tag',)
