from .genericsheetviews import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView

from .care import (
    SheetCareList,
    SheetCareCreate,
    SheetCareUpdate,
    SheetCareDetail,
    SheetCareDelete,
)

from .climate import (
    SheetClimateList,
    SheetClimateCreate,
    SheetClimateUpdate,
    SheetClimateDetail,
    SheetClimateDelete,
)

from .exposition import (
    SheetExpositionList,
    SheetExpositionCreate,
    SheetExpositionUpdate,
    SheetExpositionDetail,
    SheetExpositionDelete,
)

from .region import (
    SheetRegionList,
    SheetRegionCreate,
    SheetRegionUpdate,
    SheetRegionDetail,
    SheetRegionDelete,
)

from .sheet import (
    SheetList,
    SheetCreate,
    SheetUpdate,
    SheetDetail,
    SheetDelete,
    SheetUpvote,
    SheetDownvote,
    SheetComment,
    SheetSearch,
)

from .size import (
    SheetSizeList,
    SheetSizeCreate,
    SheetSizeUpdate,
    SheetSizeDetail,
    SheetSizeDelete,
)

from .taxonomy import (
    SheetTaxonomyList,
    SheetTaxonomyCreate,
    SheetTaxonomyUpdate,
    SheetTaxonomyDetail,
    SheetTaxonomyDelete,
)

from .taxonomyrank import (
    SheetTaxonomyRankList,
    SheetTaxonomyRankCreate,
    SheetTaxonomyRankUpdate,
    SheetTaxonomyRankDetail,
    SheetTaxonomyRankDelete,
)

from .tag import (
    SheetTagList,
    SheetTagCreate,
    SheetTagUpdate,
    SheetTagDetail,
    SheetTagDelete,
)
