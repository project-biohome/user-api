from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from json_serializer import JSONSerializer
from django.views.generic import CreateView, UpdateView

import json


class GenericBaseSheetView(LoginRequiredMixin):
    def get_template_names(self):
        if self.template_name:
            return self.template_name
        if hasattr(self, 'object_list'):
            opts = self.object_list.model._meta
        else:
            opts = self.model._meta
        return ("%s/%s%s.html" % (opts.model_name, opts.model_name, self.template_name_suffix))

    def get_object_list(self, request, classname, fields=None, use_natural_keys=False):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields if fields is None else fields
            result = JSONSerializer().serialize(self.get_queryset(), fields=fields, use_natural_foreign_keys=use_natural_keys)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(classname, self).get(request)

    def get_object_detail(self, request, pk, classname, fields=None):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields if fields is None else fields
            result = JSONSerializer().serialize([self.model.objects.get(pk=pk)], fields=fields, use_natural_foreign_keys=True)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(classname, self).get(request, pk)

class GenericSheetCreateView(CreateView):
    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            form = self.get_form()
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': form.errors.as_text()}, status=400)
        return super().post(request, *args, **kwargs)

class GenericSheetUpdateView(UpdateView):
    def post(self, request, *args, **kwargs):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            obj = self.model.objects.get(pk=kwargs['pk'])
            form = self.form_class(request.POST, instance=obj)
            if not form.is_valid():
                print(form.errors.as_text())
                return JsonResponse({'error': form.errors.as_text()}, status=400)
        return super().post(request, *args, **kwargs)
