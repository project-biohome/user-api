from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetTaxonomyRank
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetTaxonomyRankForm

from users.mixins import BiohomePermissionMixin

class GenericSheetTaxonomyRankView(GenericBaseSheetView):
    model = SheetTaxonomyRank
    form_class = SheetTaxonomyRankForm
    success_url = reverse_lazy('sheet_taxonomy_rank_list')

class SheetTaxonomyRankList(GenericSheetTaxonomyRankView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetTaxonomyRankCreate(BiohomePermissionMixin, GenericSheetTaxonomyRankView, GenericSheetCreateView):
    permission_required = 'sheets.taxonomyrank.can_add'

    pass

class SheetTaxonomyRankUpdate(BiohomePermissionMixin, GenericSheetTaxonomyRankView, GenericSheetUpdateView):
    permission_required = 'sheets.taxonomyrank.can_edit'

    pass

class SheetTaxonomyRankDetail(GenericSheetTaxonomyRankView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetTaxonomyRankDelete(BiohomePermissionMixin, GenericSheetTaxonomyRankView, DeleteView):
    permission_required = 'sheets.taxonomyrank.can_delete'

    pass
