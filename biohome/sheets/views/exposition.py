from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetExposition
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetExpositionForm

from users.mixins import BiohomePermissionMixin

class GenericSheetExpositionView(GenericBaseSheetView):
    model = SheetExposition
    form_class = SheetExpositionForm
    success_url = reverse_lazy('sheet_exposition_list')

class SheetExpositionList(GenericSheetExpositionView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetExpositionCreate(BiohomePermissionMixin, GenericSheetExpositionView, GenericSheetCreateView):
    permission_required = 'sheets.exposition.can_add'

    pass

class SheetExpositionUpdate(BiohomePermissionMixin, GenericSheetExpositionView, GenericSheetUpdateView):
    permission_required = 'sheets.exposition.can_edit'

    pass

class SheetExpositionDetail(GenericSheetExpositionView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetExpositionDelete(BiohomePermissionMixin, GenericSheetExpositionView, DeleteView):
    permission_required = 'sheets.exposition.can_delete'

    pass
