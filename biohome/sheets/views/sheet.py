from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.views.generic import (
    ListView,
    DetailView,
    DeleteView,
)
from django.views.generic.base import View, TemplateView
from django.utils.datastructures import MultiValueDictKeyError
from django.http import JsonResponse

from sheets.models import Sheet
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetForm

from users.mixins import BiohomePermissionMixin

import json

class GenericSheetView(GenericBaseSheetView):
    model = Sheet
    form_class = SheetForm
    success_url = reverse_lazy('sheet_list')

class SheetList(GenericSheetView, ListView):
    fields = [
        'name',
        'description',
        'tags',
        'image',
        'votes',
    ]

    def get_queryset(self):
        tag = self.request.GET.get('tag', None)
        if tag:
            return self.model.objects.filter(tags__in=[tag])
        search = self.request.GET.get('search', None)
        if search:
            return self.model.objects.filter(name__icontains=search)
        return super().get_queryset()

    def get(self, request):
        return self.get_object_list(request, ListView, self.fields, True)

class SheetCreate(BiohomePermissionMixin, GenericSheetView, GenericSheetCreateView):
    permission_required = 'sheets.sheet.can_add'

    pass

class SheetUpdate(BiohomePermissionMixin, GenericSheetView, GenericSheetUpdateView):
    permission_required = 'sheets.sheet.can_edit'

    pass

class SheetDetail(GenericSheetView, DetailView):
    def get(self, request, pk):
        fields = self.form_class._meta.fields + [
            'votes',
            'comments',
        ]

        return self.get_object_detail(request, pk, DetailView, fields)

class SheetDelete(BiohomePermissionMixin, GenericSheetView, DeleteView):
    permission_required = 'sheets.sheet.can_delete'

    pass

class SheetUpvote(BiohomePermissionMixin, GenericSheetView, View):
    permission_required = 'votes.can_add'

    def get(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        obj.votes.upvote_or_reset(request.user)
        return redirect('sheet_detail', pk=pk)

class SheetDownvote(GenericSheetView, View):

    def get(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        obj.votes.downvote_or_reset(request.user)
        return redirect('sheet_detail', pk=pk)

class SheetComment(GenericSheetView, View):

    def post(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            obj.comments.create(title=data['title'], comment=data['comment'], author=request.user)
            return JsonResponse({
                'response': 'comment posted successfully',
                'sheet_id': pk
            }, status=200)
        else:
            try:
                obj.comments.create(title=request.POST['title'], comment=request.POST['comment'], author=request.user)
            except MultiValueDictKeyError as e:
                return JsonResponse({'error': 'missing field %s' % str(e)}, status=400)
        return redirect('sheet_detail', pk=pk)

class SheetSearch(GenericSheetView, TemplateView):
    template_name_suffix='_search'

    def get_context_data(self, **kwargs):
        # Get tags and order by size
        rangesize = (1, 4)
        tagdic = dict()
        bigger = 1
        for obj in self.model.objects.all():
            for tag in obj.tags.all():
                if tag.tag in tagdic:
                    tagdic[tag.tag]['size'] += 1
                    if tagdic[tag.tag]['size'] > bigger:
                        bigger = tagdic[tag.tag]['size']
                else:
                    tagdic[tag.tag] = {
                        'label': tag.tag,
                        'pk': tag.pk,
                        'size': 1,
                    }
        for k in tagdic.keys():
            try:
                tagdic[k]['size'] = ((rangesize[1] - rangesize[0]) * (tagdic[k]['size'] - 1)) / float(bigger - 1) + rangesize[0]
            except ZeroDivisionError as e:
                tagdic[k]['size'] = rangesize[0]
        newdic = list()
        for k,v in tagdic.items():
            v['size'] = str(v['size']) + 'em'
            newdic.append(v)

        context = super().get_context_data(**kwargs)
        context['tags'] = sorted(newdic, key=lambda a: a['label'])
        return context
