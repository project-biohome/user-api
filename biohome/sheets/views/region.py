from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetRegion
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetRegionForm

from users.mixins import BiohomePermissionMixin

class GenericSheetRegionView(GenericBaseSheetView):
    model = SheetRegion
    form_class = SheetRegionForm
    success_url = reverse_lazy('sheet_region_list')

class SheetRegionList(GenericSheetRegionView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetRegionCreate(BiohomePermissionMixin, GenericSheetRegionView, GenericSheetCreateView):
    permission_required = 'sheets.region.can_add'

    pass

class SheetRegionUpdate(BiohomePermissionMixin, GenericSheetRegionView, GenericSheetUpdateView):
    permission_required = 'sheets.region.can_edit'

    pass

class SheetRegionDetail(GenericSheetRegionView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetRegionDelete(BiohomePermissionMixin, GenericSheetRegionView, DeleteView):
    permission_required = 'sheets.region.can_delete'

    pass
