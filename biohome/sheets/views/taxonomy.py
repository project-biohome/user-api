from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetTaxonomy
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetTaxonomyForm

from users.mixins import BiohomePermissionMixin

class GenericSheetTaxonomyView(GenericBaseSheetView):
    model = SheetTaxonomy
    form_class = SheetTaxonomyForm
    success_url = reverse_lazy('sheet_taxonomy_list')

class SheetTaxonomyList(GenericSheetTaxonomyView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetTaxonomyCreate(BiohomePermissionMixin, GenericSheetTaxonomyView, GenericSheetCreateView):
    permission_required = 'sheets.taxonomy.can_add'

    pass

class SheetTaxonomyUpdate(BiohomePermissionMixin, GenericSheetTaxonomyView, GenericSheetUpdateView):
    permission_required = 'sheets.taxonomy.can_edit'

    pass

class SheetTaxonomyDetail(GenericSheetTaxonomyView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetTaxonomyDelete(BiohomePermissionMixin, GenericSheetTaxonomyView, DeleteView):
    permission_required = 'sheets.taxonomy.can_delete'

    pass
