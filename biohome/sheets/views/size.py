from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetSize
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetSizeForm

from users.mixins import BiohomePermissionMixin

class GenericSheetSizeView(GenericBaseSheetView):
    model = SheetSize
    form_class = SheetSizeForm
    success_url = reverse_lazy('sheet_size_list')

class SheetSizeList(GenericSheetSizeView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetSizeCreate(BiohomePermissionMixin, GenericSheetSizeView, GenericSheetCreateView):
    permission_required = 'sheets.size.can_add'

    pass

class SheetSizeUpdate(BiohomePermissionMixin, GenericSheetSizeView, GenericSheetUpdateView):
    permission_required = 'sheets.size.can_edit'

    pass

class SheetSizeDetail(GenericSheetSizeView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetSizeDelete(BiohomePermissionMixin, GenericSheetSizeView, DeleteView):
    permission_required = 'sheets.size.can_delete'

    pass
