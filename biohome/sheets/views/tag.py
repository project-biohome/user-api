from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetTag
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetTagForm

from users.mixins import BiohomePermissionMixin

class GenericSheetTagView(GenericBaseSheetView):
    model = SheetTag
    form_class = SheetTagForm
    success_url = reverse_lazy('sheet_tag_list')

class SheetTagList(GenericSheetTagView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetTagCreate(BiohomePermissionMixin, GenericSheetTagView, GenericSheetCreateView):
    permission_required = 'sheets.tag.can_add'

    pass

class SheetTagUpdate(BiohomePermissionMixin, GenericSheetTagView, GenericSheetUpdateView):
    permission_required = 'sheets.tag.can_edit'

    pass

class SheetTagDetail(GenericSheetTagView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetTagDelete(BiohomePermissionMixin, GenericSheetTagView, DeleteView):
    permission_required = 'sheets.tag.can_delete'

    pass
