from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetCare
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetCareForm

from users.mixins import BiohomePermissionMixin

class GenericSheetCareView(GenericBaseSheetView):
    model = SheetCare
    form_class = SheetCareForm
    success_url = reverse_lazy('sheet_care_list')

class SheetCareList(GenericSheetCareView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetCareCreate(BiohomePermissionMixin, GenericSheetCareView, GenericSheetCreateView):
    permission_required = 'sheets.care.can_add'

    pass

class SheetCareUpdate(BiohomePermissionMixin, GenericSheetCareView, GenericSheetUpdateView):
    permission_required = 'sheets.care.can_edit'

    pass

class SheetCareDetail(GenericSheetCareView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetCareDelete(BiohomePermissionMixin, GenericSheetCareView, DeleteView):
    permission_required = 'sheets.care.can_delete'

    pass
