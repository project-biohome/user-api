from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView,
    DeleteView,
)

from sheets.models import SheetClimate
from sheets.views import GenericBaseSheetView, GenericSheetCreateView, GenericSheetUpdateView
from sheets.forms import SheetClimateForm

from users.mixins import BiohomePermissionMixin

class GenericSheetClimateView(GenericBaseSheetView):
    model = SheetClimate
    form_class = SheetClimateForm
    success_url = reverse_lazy('sheet_climate_list')

class SheetClimateList(GenericSheetClimateView, ListView):
    def get(self, request):
        return self.get_object_list(request, ListView)

class SheetClimateCreate(BiohomePermissionMixin, GenericSheetClimateView, GenericSheetCreateView):
    permission_required = 'sheets.climate.can_add'

    pass

class SheetClimateUpdate(BiohomePermissionMixin, GenericSheetClimateView, GenericSheetUpdateView):
    permission_required = 'sheets.climate.can_edit'

    pass

class SheetClimateDetail(GenericSheetClimateView, DetailView):
    def get(self, request, pk):
        return self.get_object_detail(request, pk, DetailView)

class SheetClimateDelete(BiohomePermissionMixin, GenericSheetClimateView, DeleteView):
    permission_required = 'sheets.climate.can_delete'

    pass
