from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetTaxonomyRank(models.Model):
    rank = models.CharField(
        _('rank'),
        default='kingdom',
        max_length=50,
        unique=True,
        help_text=_('The name of this taxonomy rank'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('The description of this taxonomy rank')
    )

    parent = models.ForeignKey(
        'SheetTaxonomyRank',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_('The parent of this taxonomy rank')
    )

    def natural_key(self):
        return {
            'rank': self.rank,
            'description': self.description,
            'id': self.pk,
            'parent': self.parent.natural_key() if self.parent else None,
        }

    class Meta:
        verbose_name = _('taxonomy rank')
        verbose_name_plural = _('taxonomy ranks')

    def __str__(self):
        return self.rank
