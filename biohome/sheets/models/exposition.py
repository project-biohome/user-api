from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetExposition(models.Model):
    exposition = models.CharField(
        _('exposition'),
        default='shady',
        max_length=50,
        unique=True,
        help_text=_('The exposition the plant needs'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of the exposition the plant needs')
    )

    def natural_key(self):
        return {
            'name': self.exposition,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        ordering = ['exposition']
        verbose_name = _('exposition')
        verbose_name_plural = _('expositions')

    def __str__(self):
        return self.exposition
