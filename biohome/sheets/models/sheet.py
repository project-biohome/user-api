from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.validators import validate_comma_separated_integer_list
from django.contrib.contenttypes.fields import GenericRelation

from users.models import User
from comments.fields import CommentField
from votes.fields import VoteField

from sheets.models import SheetCare
from sheets.models import SheetClimate
from sheets.models import SheetExposition
from sheets.models import SheetSize
from sheets.models import SheetTaxonomy
from sheets.models import SheetTag

from json_serializer import JSONSerializer

import json


def get_sheet_image_path(instance, filename):
    return 'sheet/{}/{}'.format(instance.name, filename)

class Sheet(models.Model):
    name = models.CharField(
        _('name'),
        max_length=250,
        db_index=True,
        unique=True,
        help_text=_('The common name of this plant,'),
    )

    binomial_name = models.CharField(
        _('binomial name'),
        max_length=500,
        blank=True,
        help_text=_('The binomial name of the plant'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('Description of the plant'),
    )

    tags = models.ManyToManyField(
        SheetTag,
        blank=True,
        help_text=_("The tags of the plant"),
    )

    maturing_in = models.CharField(
        _("maturing time"),
        max_length=10,
        validators=[validate_comma_separated_integer_list],
        help_text=_('The time it takes for this plant to be mature, in weeks'),

    )

    usage = models.TextField(
        _('usage'),
        blank=True,
        help_text=_('Usages of the plant')
    )

    image = models.ImageField(
        _('picture'),
        upload_to=get_sheet_image_path,
        null=True,
        blank=True,
        help_text=_('A picture of the plant'),
    )

    care = models.ForeignKey(
        SheetCare,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("Level of attention the plant needs through it's life"),
    )

    climate = models.ForeignKey(
        SheetClimate,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The kind of climate the plant needs')
    )

    exposition = models.ForeignKey(
        SheetExposition,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The amount of direct light the plant needs'),
    )

    size = models.ForeignKey(
        SheetSize,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The size the plant can reach when adult'),
    )

    taxonomy = models.ForeignKey(
        SheetTaxonomy,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The taxonomy of the plant'),
    )

    local_buy_start = models.DateField(
        _('local buy start'),
        help_text=_('Start of the period when you can find this plant on local'
        ' markets'),
    )

    local_buy_end = models.DateField(
        _('local buy end'),
        help_text=_('End of the period when you can find this plant on local '
        ' markets'),
    )

    plant_start = models.DateField(
        _('plantation start'),
        help_text=_('Start of the period when you can plant this plant'),
    )

    plant_end = models.DateField(
        _('plantation end'),
        help_text=_('End of the period when you can plant this plant'),
    )

    created_by = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        related_name='created_sheets',
        help_text=_('The user who created the sheet'),
    )

    updated_by = models.ForeignKey(
        User,
        null=True,
        on_delete=models.SET_NULL,
        related_name='updated_sheets',
        help_text=_('The user who last updated the sheet'),
    )

    created_on = models.DateTimeField(
        _('created on'),
        auto_now_add=True,
        help_text=_('Date and time on which the sheet was created'),
    )

    updated_on = models.DateTimeField(
        _('updated on'),
        auto_now=True,
        help_text=_('Date and time on which the sheet was last updated'),
    )

    comments = CommentField()

    votes = VoteField()

    def natural_key(self):
        fields = [
            'name',
            'description',
            'image',
            'votes',
            'tags',
        ]
        result = JSONSerializer().serialize([self], fields=fields, use_natural_foreign_keys=True)
        parsed = json.loads(result)
        fields = parsed[0]['fields']
        return {
            **fields,
            'id': self.pk,
        }

    class Meta:
        get_latest_by = 'updated_on'
        ordering = ['name']
        verbose_name = _('sheet')
        verbose_name_plural = _('sheets')

    def __str__(self):
        return self.name
