from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetSize(models.Model):
    size = models.CharField(
        _('size'),
        default='medium',
        max_length=50,
        unique=True,
        help_text=_('The size the plant can reach'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of the size the plant can reach')
    )

    def natural_key(self):
        return {
            'name': self.size,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        ordering = ['size']
        verbose_name = _('size')
        verbose_name_plural = _('sizes')

    def __str__(self):
        return self.size
