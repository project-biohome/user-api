from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetCare(models.Model):
    care = models.CharField(
        _('care'),
        default='medium',
        max_length=50,
        unique=True,
        help_text=_('The level of attention the plant needs'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of the attention needed by the plant')
    )

    def natural_key(self):
        return {
            'name': self.care,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        ordering = ['care']
        verbose_name = 'care'
        verbose_name_plural = 'cares'

    def __str__(self):
        return self.care
