from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetRegion(models.Model):
    region = models.CharField(
        _('region'),
        max_length=100,
        unique=True,
        help_text=_('Name of the region')
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of this region'),
    )

    def natural_key(self):
        return {
            'name': self.region,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        verbose_name = _('region')
        verbose_name_plural = _('regions')

    def __str__(self):
        return self.region
