from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetClimate(models.Model):
    climate = models.CharField(
        _('climate'),
        default='temperate',
        max_length=50,
        unique=True,
        help_text=_('The climate the plant needs'),
    )

    description = models.TextField(
        _('description'),
        blank=True,
        help_text=_('A short description of the climate needed by the plant')
    )

    def natural_key(self):
        return {
            'name': self.climate,
            'description': self.description,
            'id': self.pk,
        }

    class Meta:
        ordering = ['climate']
        verbose_name = _('climate')
        verbose_name_plural = _('climates')

    def __str__(self):
        return self.climate
