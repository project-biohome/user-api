from django.db import models
from django.utils.translation import ugettext_lazy as _

class SheetTag(models.Model):
    tag = models.CharField(
        _('tag'),
        max_length=50,
        unique=True,
        help_text=_('The name of the tag'),
    )

    def natural_key(self):
        return self.tag

    class Meta:
        ordering = ['tag']
        verbose_name = 'tag'
        verbose_name_plural = 'tags'

    def __str__(self):
        return self.tag
