from django.db import models
from django.utils.translation import ugettext_lazy as _

from .taxonomyrank import SheetTaxonomyRank

class SheetTaxonomy(models.Model):
    name = models.CharField(
        _('name'),
        default='default',
        max_length=250,
        unique=True,
        help_text=_('The name of this level of taxonomy'),
    )

    parent = models.ForeignKey(
        'SheetTaxonomy',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_('The parent of this level of taxonomy'),
    )

    rank = models.ForeignKey(
        SheetTaxonomyRank,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The rank of this taxonomy'),
    )

    def natural_key(self):
        return {
            'name': self.name,
            'rank': self.rank.natural_key(),
            'id': self.pk,
            'parent': self.parent.natural_key() if self.parent else None,
        }

    class Meta:
        verbose_name = _('taxonomy')
        verbose_name_plural = _('taxonomies')

    def __str__(self):
        return self.name
