from django import forms

from sheets.models import Sheet
from widgets import widgets

class SheetForm(forms.ModelForm):
    class Meta:
        model = Sheet
        fields = [
            'name',
            'binomial_name',
            'description',
            'tags',
            'maturing_in',
            'usage',
            'image',
            'care',
            'climate',
            'exposition',
            'size',
            'taxonomy',
            'local_buy_start',
            'local_buy_end',
            'plant_start',
            'plant_end',
        ]

        widgets = {
            'name': widgets.TextInput,
            'binomial_name': widgets.TextInput,
            'description': widgets.Textarea,
            'tags': widgets.SelectMultiple,
            'maturing_in': widgets.TextInput,
            'usage':  widgets.Textarea,
            'image': widgets.FileInput,
            'care': widgets.Select,
            'climate': widgets.Select,
            'exposition': widgets.Select,
            'size': widgets.Select,
            'taxonomy': widgets.Select,
            'local_buy_start': forms.widgets.SelectDateWidget,
            'local_buy_end': forms.widgets.SelectDateWidget,
            'plant_start': forms.widgets.SelectDateWidget,
            'plant_end': forms.widgets.SelectDateWidget,
        }
