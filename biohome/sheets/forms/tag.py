from django import forms

from sheets.models import SheetTag
from widgets import widgets

class SheetTagForm(forms.ModelForm):
    class Meta:
        model = SheetTag
        fields = [
            'tag',
        ]

        widgets = {
            'tag': widgets.TextInput,
        }
