from django import forms

from sheets.models import SheetTaxonomyRank
from widgets import widgets

class SheetTaxonomyRankForm(forms.ModelForm):
    class Meta:
        model = SheetTaxonomyRank
        fields = [
            'rank',
            'description',
            'parent',
        ]

        widgets = {
            'rank': widgets.TextInput,
            'description': widgets.Textarea,
            'parent': widgets.Select,
        }

    def is_valid(self):
        valid = super().is_valid()
        if not valid:
            return valid

        # check parent infinite loop
        parent = self.cleaned_data['parent']
        if parent:
            max_recursion = 30
            n = 0
            while parent and n < max_recursion:
                parent = parent.parent
                n += 1
            if n >= max_recursion:
                self.add_error('parent', 'Invalid parent chosen')
                return False

        return valid
