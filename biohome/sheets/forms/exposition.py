from django import forms

from sheets.models import SheetExposition
from widgets import widgets

class SheetExpositionForm(forms.ModelForm):
    class Meta:
        model = SheetExposition
        fields = [
            'exposition',
            'description',
        ]

        widgets = {
            'exposition': widgets.TextInput,
            'description': widgets.Textarea,
        }