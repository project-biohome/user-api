from django import forms

from sheets.models import SheetSize
from widgets import widgets

class SheetSizeForm(forms.ModelForm):
    class Meta:
        model = SheetSize
        fields = [
            'size',
            'description',
        ]

        widgets = {
            'size': widgets.TextInput,
            'description': widgets.Textarea,
        }
