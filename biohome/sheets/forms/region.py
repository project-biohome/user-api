from django import forms

from sheets.models import SheetRegion
from widgets import widgets

class SheetRegionForm(forms.ModelForm):
    class Meta:
        model = SheetRegion
        fields = [
            'region',
            'description',
        ]

        widgets = {
            'region': widgets.TextInput,
            'description': widgets.Textarea,
        }