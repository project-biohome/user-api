from django import forms

from sheets.models import SheetClimate
from widgets import widgets

class SheetClimateForm(forms.ModelForm):
    class Meta:
        model = SheetClimate
        fields = [
            'climate',
            'description',
        ]

        widgets = {
            'climate': widgets.TextInput,
            'description': widgets.Textarea,
        }
