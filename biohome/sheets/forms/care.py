from django import forms

from sheets.models import SheetCare
from widgets import widgets

class SheetCareForm(forms.ModelForm):
    class Meta:
        model = SheetCare
        fields = [
            'care',
            'description',
        ]

        widgets = {
            'care': widgets.TextInput,
            'description': widgets.Textarea,
        }
