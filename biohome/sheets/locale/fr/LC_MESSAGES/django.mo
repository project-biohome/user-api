��    @        Y         �     �  8   �  6   �  5     3   >  "   r  ,   �  1   �     �  @     /   N  4   ~     �  A   �  1     *   :     e     �     �  %   �     �  #   	  &   (	  "   O	     r	  $   �	      �	     �	     �	  '   
     6
  7   P
     �
  #   �
     �
     �
     �
     �
     �
  
          
        %     1     ?     O     ]     b     j     y     �     �     �     �     �     �     �  
   �     �     �     �  
   �     �  j  �     c  =   }  8   �  >   �  C   3  '   w  5   �  R   �     (  Y   D  0   �  $   �     �  \     3   c  8   �     �  !   �       '   0  %   X  -   ~  &   �      �     �  #     !   7     Y  )   t  )   �     �  D   �  &   *  6   Q     �     �  	   �     �     �  	   �     �  
   �     �     �     
          3     7     =     O     d     i     q     z     �     �     �  
   �  	   �     �     �     �     �                      5   :   =      "   @          ?   -   /          0   #       3                (      6   9         $      <               2       ;   '      8             ,   4      +                                
   *          )   7          	   .                           &       %      1                  !   >        A picture of the plant A short description of the attention needed by the plant A short description of the climate needed by the plant A short description of the exposition the plant needs A short description of the size the plant can reach A short description of this region Date and time on which the sheet was created Date and time on which the sheet was last updated Description of the plant End of the period when you can find this plant on local  markets End of the period when you can plant this plant Level of attention the plant needs through it's life Name of the region Start of the period when you can find this plant on local markets Start of the period when you can plant this plant The amount of direct light the plant needs The binomial name of the plant The climate the plant needs The common name of this plant, The description of this taxonomy rank The exposition the plant needs The kind of climate the plant needs The level of attention the plant needs The name of this level of taxonomy The name of this taxonomy rank The parent of this level of taxonomy The parent of this taxonomy rank The rank of this taxonomy The size the plant can reach The size the plant can reach when adult The taxonomy of the plant The time it takes for this plant to be mature, in weeks The user who created the sheet The user who last updated the sheet Usages of the plant binomial name care climate climates created on description exposition expositions local buy end local buy start maturing time name picture plantation end plantation start rank region regions sheet sheets size sizes taxonomies taxonomy taxonomy rank taxonomy ranks updated on usage Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-08-16 10:40+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Une image de cette plante Une courte description de l'attention demandée par la plante Une courte description du climat dont la plante a besoin Une courte description de l'exposition dont la plante a besoin Une courte description de la taille que peut atteindre cette plante Une courte description de cette région Date et heure à laquelle cette fiche a été créée Date et heure à laquelle cette fiche à été mise à jour pour la dernière fois Description de cette plante Fin de la période durant laquelle l'on peut trouver cette plante sur les marchés locaux Fin de la période de plantation de cette plante Attention demandée par cette plante Nom de la région Début de la période durant laquelle l'on peut trouver cette plante sur les marchés locaux Début de la période de plantation de cette plante La quantité de lumière directe dont la plante a besoin Le nom binomial de cette plante Le climat dont la plante a besoin Le nom commun de cette plante Une description de ce rang de taxonomie L'exposition dont la plante à besoin Le genre de climat dont cette plante a besoin L'attention demandée par cette plante Le nom de ce niveau de taxonomie Le nom de ce rang de taxonomie Le parent de ce niveau de taxonomie Le parent de ce rang de taxonomie Le rang de cette taxonomie La taille que cette plante peut atteindre La taille que cette plante peut atteindre La taxonomie de cette plante Le temps que prend cette plante à arriver à maturité, en semaines L'utilisateur ayant créé cette fiche L'utilisateur ayant mis à jour cette fiche en dernier Utilisations de cette plante nom binomial attention climat climats créé le description exposition expositions fin d'achat local début d'achat local temps de maturation nom image fin de plantation début de plantation rang région régions fiche fiches taille tailles taxonomies taxonomie rang de taxonomie rangs de taxonomie mise à jour le utilisation 