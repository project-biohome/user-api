from django.apps import AppConfig


class BiomesConfig(AppConfig):
    name = 'biomes'

    def ready(self):
        import biomes.signals
