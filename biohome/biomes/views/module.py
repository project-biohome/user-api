from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from biomes.models import BiomeModule
from biomes.forms import BiomeModuleForm

from biomes.views import (
    GenericBiomeView,
    GenericBiomeListView,
    GenericBiomeCreateView,
    GenericBiomeUpdateView,
    GenericBiomeDetailView,
)

from json_serializer import errors_to_json
from users.mixins import BiohomePermissionMixin

import json

class BiomeModuleView(LoginRequiredMixin):
    model = BiomeModule
    form_class = BiomeModuleForm
    success_url = reverse_lazy('module_list')

class BiomeModuleList(BiomeModuleView, GenericBiomeListView):
    def get_queryset(self):
        user = self.request.user
        queryset = self.model.objects.all()
        if user.is_superuser:
            return queryset
        return queryset.filter(biome__owner=user.pk)

    pass

class BiomeModuleCreate(BiomeModuleView, GenericBiomeCreateView):
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = self.form_class(data=data)
        if form.is_valid():
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                module = form.instance
                module.save()
                return JsonResponse({
                    'response': 'module added successfully',
                    'module_id' : module.pk
                }, status=200)
            else:
                return self.form_valid(form)
        else:
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'error': errors_to_json(form.errors)
                }, status=400)
            else:
                return self.form_invalid(form)

class BiomeModuleUpdate(BiomeModuleView, GenericBiomeUpdateView):
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=self.object)
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = self.form_class(data=data, instance=self.object)
        if form.is_valid():
            module = form.instance
            module.save()
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'response': 'biome updated successfully',
                    'module_id': module.pk
                }, status=200)
            else:
                self.object = module
                redir = request.POST.get('next')
                if redir:
                    self.success_url = redir
                return HttpResponseRedirect(self.get_success_url())
        else:
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'error': form.errors.as_text()
                }, status=400)
            else:
                return self.form_invalid(form)

class BiomeModuleDetail(BiomeModuleView, GenericBiomeDetailView):
    pass

class BiomeModuleDelete(BiomeModuleView, GenericBiomeView, DeleteView):
    pass
