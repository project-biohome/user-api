from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import CreateView

from biomes.forms.events import BiomeCultureEventForm
from biomes.models.events import BiomeCultureEvent, CULTURE_START, CULTURE_STOP

class BiomeCultureEventView(CreateView):
    form_class = BiomeCultureEventForm
    model = BiomeCultureEvent

    def get_success_url(self):
        return reverse('biome_detail', args=[self.object.biome.pk])

    def form_valid(self, form):
        self.object = form.save()
        print(type(self.object.biome.program))
        self.object.biome.program = self.object.program
        self.object.biome.save()
        return HttpResponseRedirect(self.get_success_url())
