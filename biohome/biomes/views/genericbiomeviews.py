from django.core import serializers
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import (
    ListView,
    CreateView,
    UpdateView,
    DetailView
)

from biomes.serializers import BiomeSerializer

from json_serializer import errors_to_json

import json

class GenericBiomeView:
    def get_template_names(self):
        if self.template_name:
            return self.template_name
        if hasattr(self, 'object_list'):
            opts = self.object_list.model._meta
        else:
            opts = self.model._meta
        return ('%s/%s%s.html' % (opts.model_name, opts.model_name, self.template_name_suffix))

class GenericBiomeListView(GenericBiomeView, ListView):
    def get_queryset(self):
        user = self.request.user
        queryset = self.model.objects.all()
        if user.is_superuser:
            return queryset
        return queryset.filter(owner=user.id)

    def get(self, request):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields
            result = serializers.serialize('json', self.get_queryset(), fields=fields)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(ListView, self).get(request)

class GenericBiomeCreateView(GenericBiomeView, CreateView):
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = self.form_class(data=data)
        if form.is_valid():
            biome = form.instance
            biome.save()
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'response': 'biome added successfully',
                    'biome_id' : biome.pk
                }, status=200)
            else:
                self.object = biome
                return HttpResponseRedirect(self.get_success_url())
        else:
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'error': errors_to_json(form.errors)
                }, status=400)
            else:
                return self.form_invalid(form)

class GenericBiomeUpdateView(GenericBiomeView, UpdateView):
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.form_class(request.POST, instance=self.object)
        if request.content_type == 'application/json':
            data = json.loads(request.body.decode('utf-8'))
            form = self.form_class(data=data, instance=self.object)
        if form.is_valid():
            biome = form.instance
            biome.save()
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'response': 'biome updated successfully',
                    'biome_id' : biome.pk
                }, status=200)
            else:
                self.object = biome
                return HttpResponseRedirect(self.get_success_url())
        else:
            if request.META.get('HTTP_ACCEPT') == 'application/json':
                return JsonResponse({
                    'error': form.errors.as_text()
                }, status=400)
            else:
                return self.form_invalid(form)

class GenericBiomeDetailView(GenericBiomeView, DetailView):
    def get(self, request, pk):
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            fields = self.form_class._meta.fields
            result = BiomeSerializer().serialize([self.model.objects.get(pk=pk)], fields=fields, use_natural_foreign_keys=True)
            parsed = json.loads(result)
            return JsonResponse(parsed, safe=False)
        return super(DetailView, self).get(request)
