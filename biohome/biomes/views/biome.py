from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import DeleteView
from django.http import HttpResponseRedirect
from django.forms.utils import ErrorList
from django.utils.translation import ugettext_lazy as _

from biomes.models import Biome, BiomeModule
from biomes.forms import BiomeForm, BiomePairingForm

from biomes.views import (
    GenericBiomeView,
    GenericBiomeListView,
    GenericBiomeCreateView,
    GenericBiomeUpdateView,
    GenericBiomeDetailView,
)

from biomes.models.events import (
    BiomeReadingEvent,
    BiomeReadingEventData,
)

class BiomeView(LoginRequiredMixin):
    model = Biome
    form_class = BiomeForm
    success_url = reverse_lazy('biome_list')

class BiomeList(BiomeView, GenericBiomeListView):

    def get_modules(self, biome):
        modules = BiomeModule.objects.filter(biome=biome.pk)
        modules = [m for m in modules]
        return modules

    def get_context_data(self, **kwargs):
        context = super(GenericBiomeListView, self).get_context_data(**kwargs)
        if 'object_list' in context:
            context['modules'] = []
            for biome in context['object_list']:
                context['modules'].append(len(self.get_modules(biome)))
        return context

class BiomeCreate(BiomeView, GenericBiomeCreateView):
    pass

class BiomePairing(BiomeView, GenericBiomeCreateView):
    template_name_suffix = '_pairing'
    form_class = BiomePairingForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        form = self.get_form()
        self.object = form.instance
        token = form['token'].value()

        # Invalid token
        if len(token) is not 6:
            if form._errors is None:
                form._errors = {}
            errors = form._errors.setdefault('token', ErrorList())
            errors.append(_('Invalid token, must be a 6 chars len string'))
            return self.form_invalid(form)

        queryset = self.model.objects.filter(token=token)
        # Unknown token
        if len(queryset) is not 1:
            if form._errors is None:
                form._errors = {}
            errors = form._errors.setdefault('token', ErrorList())
            errors.append(_('This token doesn\'t exist'))
            return self.form_invalid(form)

        biome = queryset[0]
        biome.token = None
        biome.owner = request.user
        biome.save()
        return HttpResponseRedirect(self.get_success_url())

class BiomeUpdate(BiomeView, GenericBiomeUpdateView):
    pass

class BiomeDetail(BiomeView, GenericBiomeDetailView):
    def get_readings(self, biome):
        readingqs = BiomeReadingEvent.objects.filter(biome=biome.pk).order_by('-timestamp')[:12]
        readings = dict()
        for reading in readingqs:
            dataqs = BiomeReadingEventData.objects.filter(reading=reading.pk, module__active=True).select_related('module__type')
            for data in dataqs:
                datatype = str(data.module.type.name)
                if datatype not in readings:
                    readings[datatype] = [0, data.avg]
                else:
                    x = int(len(readings[datatype]) / 2)
                    readings[datatype].extend([len(readings[datatype]) / 2, data.avg])
        return readings

    def get_modules(self, biome):
        modules = BiomeModule.objects.filter(biome=biome.pk)
        modules = [m for m in modules]
        return modules

    def get_context_data(self, **kwargs):
        context = super(GenericBiomeDetailView, self).get_context_data(**kwargs)
        if 'object' in context:
            biome = context['object']
            context['readings'] = self.get_readings(biome)
            context['modules'] = self.get_modules(biome)
        return context

class BiomeDelete(BiomeView, GenericBiomeView, DeleteView):
    pass
