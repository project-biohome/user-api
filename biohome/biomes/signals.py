from django.db.models.signals import post_save, pre_save, post_delete
from django.dispatch import receiver

from biomes.models import Biome
from biomes.models import BiomeModule
from biomes.models import (BiomeModuleEvent, MODULE_ACTIVATE,
                           MODULE_DEACTIVATE, MODULE_ADD)

import requests


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@receiver(pre_save, sender=Biome)
def pre_biome_save(sender, instance, **kwargs):
    if instance.token is None and instance.owner is None:
        instance.generate_token()

@static_vars(action=-1)
@receiver(post_save, sender=BiomeModule)
def post_module_save(sender, instance, **kwargs):
    if post_module_save.action != -1:
        event = BiomeModuleEvent(biome=instance.biome, module=instance,
                                 action=post_module_save.action)
        event.save()
        try:
            print('GET request with http://biome:80/module-event/{}'.format(event.pk))
            requests.get('http://biome:80/module-event/{}'.format(event.pk))
        except BaseException as e:
            print(e)
    post_module_save.action = -1


@receiver(pre_save, sender=BiomeModule)
def pre_module_save(sender, instance, **kwargs):
    if instance.pk:
        old = BiomeModule.objects.get(pk=instance.pk)
        if old.active != instance.active:
            if old.active == False and instance.active == True:
                post_module_save.action = MODULE_ACTIVATE
            else:
                post_module_save.action = MODULE_DEACTIVATE
    else:
        post_module_save.action = MODULE_ADD
