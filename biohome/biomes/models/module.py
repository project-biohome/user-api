from django.db import models
from django.utils.translation import ugettext_lazy as _

from users.models import User
from biomes.models import Biome, BiomeModuleType

class BiomeModule(models.Model):
    type = models.ForeignKey(
        BiomeModuleType,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The module's type"),
    )

    biome = models.ForeignKey(
        Biome,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_("The biome this module is attached to"),
    )

    active = models.BooleanField(
        _('is active'),
        default=False,
        help_text=_('Should the module be active'),
    )

    class Meta:
        ordering = ['type', 'active']
        verbose_name = _('biome module')
        verbose_name_plural = _('biome modules')

    def __str__(self):
        if self.type:
            return str(self.type)
        return _('module')

    def natural_key(self):
        ret = {
            'type': self.type.natural_key(),
            'active': self.active,
            'id': self.pk,
        }
        if self.biome:
            ret['biome'] = self.biome.natural_key()
        return ret
