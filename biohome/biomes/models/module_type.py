from django.db import models
from django.utils.translation import ugettext_lazy as _


class BiomeModuleType(models.Model):
    name = models.CharField(
        _('name'),
        max_length=80,
        help_text=_("The module's name"),
    )

    version = models.FloatField(
        _('version'),
        blank=True,
        help_text=_("The module's version"),
    )

    class Meta:
        ordering = ['name', 'version']
        verbose_name = _('module type')
        verbose_name_plural = _('module types')

    def __str__(self):
        return "{} ({})".format(str(self.name), str(self.version))

    def natural_key(self):
        return {
            'name': self.name,
            'version': self.version,
            'id': self.pk,
        }
