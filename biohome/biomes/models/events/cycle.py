from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from biomes.models.events import BiomeAbstractEvent
from programs.models import ProgramCycle

CYCLE_START_NEW = 1
CYCLE_UPDATE = 2

class BiomeCycleEvent(BiomeAbstractEvent):
    cycle = models.ForeignKey(
        ProgramCycle,
        null=True,
        on_delete=models.CASCADE,
        help_text=_('The updated cycle'),
    )

    def __str__(self):
        if self.action and self.cycle:
            if self.action == CYCLE_START_NEW:
                return "{} {}".format(str(self.cycle), __('has started'))
            if self.action == CYCLE_UPDATE:
                return "{} {}".format(str(self.cycle), __('was updated'))
        return __('Incomplete event')

    class Meta:
        verbose_name = _('biome cycle event')
        verbose_name_plural = _('biome cycle events')
