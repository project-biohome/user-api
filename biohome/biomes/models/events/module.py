from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from biomes.models.events import BiomeAbstractEvent
from biomes.models import BiomeModule

MODULE_ADD = 1
MODULE_REMOVE = 2
MODULE_ACTIVATE = 3
MODULE_DEACTIVATE = 4

class BiomeModuleEvent(BiomeAbstractEvent):
    module = models.ForeignKey(
        BiomeModule,
        null=True,
        on_delete=models.CASCADE,
        help_text=_('The module that changed state'),
    )

    def __str__(self):
        if self.action and self.module:
            if self.action == MODULE_ADD:
                msg = __("added")
            if self.action == MODULE_REMOVE:
                msg = __("removed")
            if self.action == MODULE_ACTIVATE:
                msg = __("activated")
            if self.action == MODULE_DEACTIVATE:
                msg = __("deactivated")
            return "{} {} {}".format(str(self.module), __("was"), msg)
        return __('Incomplete event')

    class Meta:
        verbose_name = _('biome module event')
        verbose_name_plural = _('biome module events')
