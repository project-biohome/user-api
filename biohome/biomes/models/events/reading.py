from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from biomes.models import BiomeModule
from biomes.models.events import BiomeAbstractEvent

class BiomeReadingEvent(BiomeAbstractEvent):
    def __str__(self):
        return str(self.timestamp)

    class Meta:
        verbose_name = _('biome reading event')
        verbose_name_plural = _('biome readings events')

class BiomeReadingEventData(models.Model):
    mini = models.FloatField(
        _('mininum'),
        help_text=_('The lowest value read'),
    )

    avg = models.FloatField(
        _('average'),
        help_text=_('The average value read'),
    )

    maxi = models.FloatField(
        _('maximum'),
        help_text=_('The highest value read'),
    )

    reading = models.ForeignKey(
        BiomeReadingEvent,
        null=True,
        on_delete=models.CASCADE,
        help_text=_('The reading this data corresponds to'),
    )

    module = models.ForeignKey(
        BiomeModule,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The module this data was taken from')
    )

    class Meta:
        verbose_name = _('biome reading event data')
        verbose_name_plural = _('biome readings event datas')


    def __str__(self):
        if self.module:
            return "{} ({}/{}/{}) {} {}".format(
                __('Received'),
                self.mini,
                self.avg,
                self.maxi,
                __('from'),
                str(self.module)
            )
        return __('Incomplete data')
