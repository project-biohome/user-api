from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __

from .abstract import BiomeAbstractEvent
from programs.models import Program

CULTURE_START = 1
CULTURE_STOP = 2

class BiomeCultureEvent(BiomeAbstractEvent):
    program = models.ForeignKey(
        Program,
        null=True,
        on_delete=models.CASCADE,
        help_text=_('The program used for the culture'),
    )

    def __str__(self):
        if self.action and self.program:
            if self.action == CULTURE_START:
                return "{} {}".format(str(self.program), __('has started'))
            if self.action == CULTURE_STOP:
                return "{} {}".format(str(self.program), __('has stopped'))
        return __('Incomplete event')

    class Meta:
        verbose_name = _('biome culture event')
        verbose_name_plural = _('biome culture events')
