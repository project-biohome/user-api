from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __
from django.utils import timezone

from biomes.models import Biome

class BiomeAbstractEvent(models.Model):
    biome = models.ForeignKey(
        Biome,
        null=True,
        on_delete=models.SET_NULL,
        help_text=_('The associated biome'),
    )

    action = models.PositiveSmallIntegerField(
        _('action'),
        blank=True,
        null=True,
        help_text=_('The action that was performed'),
    )

    timestamp = models.DateTimeField(
        _('timestamp'),
        default=timezone.now,
        help_text=_('Date and time on which the data was receiveds'),
    )

    class Meta:
        abstract = True
        ordering = ['timestamp']
        verbose_name = _('biome event')
        verbose_name_plural = _('biome events')

    def __str__(self):
        return __('Empty event')
