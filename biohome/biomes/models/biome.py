from django.db import models
from django.utils.translation import ugettext_lazy as _

from users.models import User
from programs.models import Program, ProgramCycle

import random
import string

class Biome(models.Model):
    name = models.CharField(
        _('name'),
        max_length=80,
        null=True,
        help_text=_("The biome's name"),
    )

    token = models.CharField(
        _('token'),
        max_length=250,
        null=True,
        blank=True,
        help_text=_("The biome's pairing token"),
    )

    ip = models.GenericIPAddressField(
        _('ip'),
        null=True,
        blank=True,
        help_text=_("The biome's IP"),
    )

    owner = models.ForeignKey(
        User,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_("The biome's owner"),
    )

    program = models.ForeignKey(
        Program,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_("The biome's current program"),
    )

    program_start = models.DateTimeField(
        _('program started on'),
        blank=True,
        null=True,
        help_text=_('The date and time on which the current program was'
                    'started'),
    )

    current_cycle = models.ForeignKey(
        ProgramCycle,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text=_("The current program's cycle"),
    )

    class Meta:
        ordering = ['ip', 'owner']
        verbose_name = _('biome')
        verbose_name_plural = _('biomes')

    def __str__(self):
        return self.name

    def natural_key(self):
        return {
            'name': self.name,
            'id': self.pk,
        }

    def get_modules(self):
        from biomes.models import BiomeModule
        return BiomeModule.objects.filter(biome=self.pk)

    def generate_token(self):
        self.token = ''.join(random.choice(string.ascii_uppercase) for _ in range (6))
        if len(Biome.objects.filter(token=self.token)) > 0:
            self.generate_token()
