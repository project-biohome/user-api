from json_serializer import JSONSerializer
from biomes.models import Biome, BiomeModule

class BiomeSerializer(JSONSerializer):
    def end_object(self, obj):
        concrete_model = obj._meta.concrete_model
        local_fields = concrete_model._meta.local_fields
        many_to_many = concrete_model._meta.many_to_many
        fields = concrete_model._meta.get_fields()
        fields = [x for x in [x for x in fields if x not in many_to_many] if x not in local_fields]
        for field in fields:
            if hasattr(field, 'attname'):
                if self.selected_fields is None or field.attname in self.selected_fields:
                    name = field.name
                    field = getattr(obj, field.name)
                    if hasattr(field, 'toJSON'):
                        self._current[name] = field.toJSON(self)
                    else:
                        self._current[name] = None
        if type(obj) == Biome:
            modules = obj.get_modules()
            self._current['modules'] = list()
            for module in modules:
                self._current['modules'].append(module.natural_key())
        super(JSONSerializer, self).end_object(obj)
