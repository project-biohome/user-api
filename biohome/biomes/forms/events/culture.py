from django import forms

from biomes.models import BiomeCultureEvent

class BiomeCultureEventForm(forms.ModelForm):
    class Meta:
        model = BiomeCultureEvent
        fields = [
            'biome',
            'action',
            'program',
        ]
