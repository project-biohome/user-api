from django import forms

from biomes.models import BiomeModule
from widgets import widgets

class BiomeModuleForm(forms.ModelForm):
    class Meta:
        model = BiomeModule
        fields = [
            'type',
            'active',
            'biome',
        ]

        widgets = {
            'type': widgets.widgets.Select,
            'active': widgets.CheckboxInput,
            'biome': widgets.widgets.Select,
        }
