from django import forms

from biomes.models import BiomeModuleType
from widgets import widgets

class BiomeModuleTypeForm(forms.ModelForm):
    class Meta:
        model = BiomeModuleType
        fields = [
            'name',
            'version',
        ]

        widgets = {
            'name': widgets.TextInput,
            'active': widgets.CheckboxInput,
        }
