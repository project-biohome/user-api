from django import forms

from biomes.models import Biome
from widgets import widgets

class BiomeForm(forms.ModelForm):
    class Meta:
        model = Biome
        fields = [
            'name',
            'ip',
            'program',
            'current_cycle',
        ]

        widgets = {
            'name': widgets.TextInput,
            'ip': widgets.TextInput,
            'program': widgets.Select,
            'current_cycle': widgets.Select,
        }

class BiomePairingForm(forms.ModelForm):
    class Meta:
        model = Biome
        fields = [
            'token',
        ]

        widgets = {
            'token': widgets.TextInput,
        }
