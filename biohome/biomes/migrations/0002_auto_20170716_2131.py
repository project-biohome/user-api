# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-07-16 21:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('biomes', '0001_initial'),
        ('programs', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='biome',
            name='current_cycle',
            field=models.ForeignKey(blank=True, help_text="The current program's cycle", null=True, on_delete=django.db.models.deletion.SET_NULL, to='programs.ProgramCycle'),
        ),
        migrations.AddField(
            model_name='biome',
            name='modules',
            field=models.ManyToManyField(blank=True, help_text="The biome's current modules", to='biomes.BiomeModule'),
        ),
    ]
