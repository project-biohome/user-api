from django.contrib import admin
from django.db import models

from biomes.models import Biome, BiomeModuleType, BiomeModule
from biomes.models.events import (BiomeCultureEvent, BiomeCycleEvent,
    BiomeModuleEvent, BiomeReadingEvent, BiomeReadingEventData)

@admin.register(BiomeModule)
class BiomeModuleAdmin(admin.ModelAdmin):
    list_display = ('id', 'type', 'active', 'biome')

@admin.register(BiomeModuleType)
class BiomeModuleTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'version')

@admin.register(Biome)
class BiomeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'ip', 'owner', 'program', 'current_cycle')

@admin.register(BiomeCultureEvent)
class BiomeCultureEventAdmin(admin.ModelAdmin):
    list_display = ('biome', 'action', 'program', 'timestamp')

@admin.register(BiomeCycleEvent)
class BiomeCycleEventAdmin(admin.ModelAdmin):
    list_display = ('biome', 'action', 'cycle', 'timestamp')

@admin.register(BiomeModuleEvent)
class BiomeModuleEventAdmin(admin.ModelAdmin):
    list_display = ('biome', 'action', 'module', 'timestamp')

@admin.register(BiomeReadingEvent)
class BiomeReadingEventAdmin(admin.ModelAdmin):
    list_display = ('biome', 'action', 'timestamp')

@admin.register(BiomeReadingEventData)
class BiomeReadingEventDataAdmin(admin.ModelAdmin):
    list_display = ('module', 'mini', 'avg', 'maxi', 'reading')
