from django.conf.urls import url, include
from django.views.generic import RedirectView

from biomes.views import (
    BiomeList,
    BiomeCreate,
    BiomePairing,
    BiomeUpdate,
    BiomeDetail,
    BiomeDelete,

    BiomeModuleList,
    BiomeModuleCreate,
    BiomeModuleUpdate,
    BiomeModuleDetail,
    BiomeModuleDelete,

    BiomeCultureEventView,
)

biome_patterns = [
    url(r'^$', BiomeList.as_view(), name='biome_list'),
    url(r'^add$', BiomeCreate.as_view(), name='biome_create'),
    url(r'^pair$', BiomePairing.as_view(), name='biome_pairing'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="biome_detail")),
    url(r'^(?P<pk>\d+)/update$', BiomeUpdate.as_view(), name='biome_update'),
    url(r'^(?P<pk>\d+)/details$', BiomeDetail.as_view(), name='biome_detail'),
    url(r'^(?P<pk>\d+)/delete$', BiomeDelete.as_view(), name='biome1_delete'),
]

module_patterns = [
    url(r'^$', BiomeModuleList.as_view(), name='module_list'),
    url(r'^add$', BiomeModuleCreate.as_view(), name='module_create'),
    url(r'^(?P<pk>\d+)?/$', RedirectView.as_view(pattern_name="module_detail")),
    url(r'^(?P<pk>\d+)/update$', BiomeModuleUpdate.as_view(), name='module_update'),
    url(r'^(?P<pk>\d+)/details$', BiomeModuleDetail.as_view(), name='module_detail'),
    url(r'^(?P<pk>\d+)/delete$', BiomeModuleDelete.as_view(), name='module1_delete'),
]

event_patterns = [
    url(r'^culture$', BiomeCultureEventView.as_view(), name='culture_event'),
]

urlpatterns = [
   url(r'^', include(biome_patterns)),
   url(r'^module/', include(module_patterns)),
   url(r'^event/', include(event_patterns)),
]
