from django.views.generic import TemplateView

from biomes.models import Biome, BiomeModule
from comments.models import Comment

class DashboardView(TemplateView):
    http_method_names = [u'get', u'head', u'options', u'trace']
    template_name = 'dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(TemplateView, self).get_context_data(**kwargs)
        context['object_list'] = self.get_biomes()
        context['last_comments'] = self.get_last_comments()
        if 'object_list' in context:
            context['modules'] = []
            for biome in context['object_list']:
                context['modules'].append(len(self.get_modules(biome)))
        return context

    def get_biomes(self):
        user = self.request.user
        queryset = Biome.objects.all()
        return queryset.filter(owner=user.pk)

    def get_last_comments(self):
        user = self.request.user
        queryset = Comment.objects.all()
        return queryset.filter(author=user.pk).order_by('-created_on')[:5]

    def get_modules(self, biome):
        modules = BiomeModule.objects.filter(biome=biome.pk)
        modules = [m for m in modules]
        return modules