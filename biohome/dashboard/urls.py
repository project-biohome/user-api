from django.conf.urls import url, include
from django.views.generic import RedirectView

from dashboard.views import DashboardView

urlpatterns = [
   url(r'^$', DashboardView.as_view(), name='dashboard'),
]
